-- phpMyAdmin SQL Dump
-- version 4.0.10.14
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Dec 24, 2016 at 08:35 AM
-- Server version: 5.5.52-cll-lve
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `myopos_rentuser`
--

-- --------------------------------------------------------

--
-- Table structure for table `rs_tbl_admin`
--

CREATE TABLE IF NOT EXISTS `rs_tbl_admin` (
  `admin_id` int(10) NOT NULL,
  `username` varchar(50) NOT NULL,
  `pass` varchar(100) NOT NULL DEFAULT '',
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `email` varchar(200) NOT NULL,
  `phone` varchar(25) DEFAULT NULL,
  `designation` varchar(255) DEFAULT NULL,
  `last_login_date` datetime DEFAULT NULL,
  `last_login_ip` varchar(30) DEFAULT NULL,
  `menu_id` varchar(200) DEFAULT NULL,
  `mem_type` int(2) DEFAULT '1' COMMENT '1=>Main Admin, 2=>Sub Admin',
  PRIMARY KEY (`admin_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rs_tbl_admin`
--

INSERT INTO `rs_tbl_admin` (`admin_id`, `username`, `pass`, `first_name`, `last_name`, `email`, `phone`, `designation`, `last_login_date`, `last_login_ip`, `menu_id`, `mem_type`) VALUES
(1, 'admin', 'a3f7a3d050ecaf39210582a7c0e747e0', 'Numan', 'Tahir', 'numan_tahir1@live.com', '', '', '2016-12-14 08:00:53', '62.209.14.83', NULL, 2);

-- --------------------------------------------------------

--
-- Table structure for table `rs_tbl_adminmenu`
--

CREATE TABLE IF NOT EXISTS `rs_tbl_adminmenu` (
  `menu_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `menu_title` varchar(255) NOT NULL,
  `menu_link` varchar(255) NOT NULL,
  `menu_order` smallint(4) NOT NULL DEFAULT '0',
  `frm_status` int(2) DEFAULT '1',
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rs_tbl_adminmenu`
--

INSERT INTO `rs_tbl_adminmenu` (`menu_id`, `parent_id`, `menu_title`, `menu_link`, `menu_order`, `frm_status`) VALUES
(1, 0, 'Content', './?p=sitecms_mgmt', 1, 1),
(2, 0, 'Help', './?p=sitehelp_mgmt', 2, 1),
(3, 0, 'Customer', './?p=customer_mgmt', 3, 1),
(4, 3, 'User Message', './?p=message_mgmt', 2, 1),
(9, 0, 'Settings', './?p=configuration', 8, 1),
(11, 9111111, 'Cron Management', './?p=twitter_cron_mgmt', 2, 1),
(12, 9111111, 'Country Management', './?p=country_mgmt', 4, 1),
(13, 9, 'Change Password', './?p=change_password', 5, 1),
(14, 0, 'Reports', './?p=reports_mgmt', 6, 1),
(15, 1, 'Content', './?p=sitecms_form', 0, 2),
(16, 9, 'E-mail Template', './?p=etemplate_mgmt', 6, 1),
(17, 3, 'Customer', './?p=customer_form', 1, 2),
(33, 9, 'Admin User Management', './?p=admin_user_mgmt', 7, 1),
(34, 9, 'Admin User Form', './?p=admin_user_form', 8, 2),
(35, 3, 'Newsletter Subscriber', './?p=subscribers', 3, 1),
(39, 6, 'PickBox View', './?p=pickbox_view', 11, 2),
(40, 3, 'Message Form', './?P=message_form', 6, 2),
(45, 9, 'Etemplate Form', './?p=etemplate_form', 29, 2),
(46, 9, 'Admin Help', './?p=help_mgmt', 8, 1),
(47, 9, 'Admin Help Form', './?p=help_form', 30, 2),
(68, 2, 'Faq', './?p=faq_mgmt', 3, 1),
(69, 2, 'faq_form', './?p=faq_form', 4, 2),
(70, 2, 'Help & Faq Category', './?p=hf_category_mgmt', 5, 1),
(71, 2, 'hf_category_form', './?p=hf_category_form', 6, 2),
(72, 2, 'sitehelp_form', './?p=sitehelp_form', 1, 2),
(73, 0, 'Product', './?p=product_mgmt', 4, 1),
(74, 3, 'gallery_mgmt', './?p=gallery_mgmt', 4, 2),
(75, 3, 'customer_view', './?p=customer_view', 5, 2),
(76, 73, 'product_form', './?p=product_form', 2, 2),
(77, 0, 'Order', './?p=order_mgmt', 5, 1),
(78, 0, 'Supprot', './?p=support_mgmt', 7, 1),
(79, 78, 'Support form', './?p=support_form', 8, 2),
(80, 3, 'Customer Type', './?p=customer_type_mgmt', 8, 1),
(81, 3, 'Type Option', './?p=add_type_mgmt', 9, 2),
(82, 73, 'Category', './?p=category', 10, 1),
(83, 73, 'category_form', './?p=category_form', 11, 2),
(84, 9, 'Home Slider', './?p=slider', 31, 1),
(85, 9, 'Home Slider', './?p=slider_form', 32, 2);

-- --------------------------------------------------------

--
-- Table structure for table `rs_tbl_car_lease`
--

CREATE TABLE IF NOT EXISTS `rs_tbl_car_lease` (
  `lease_id` int(11) NOT NULL,
  `lease_type` int(2) NOT NULL DEFAULT '1' COMMENT '1=>Personal, 2=>Corporate',
  `full_name` varchar(200) DEFAULT NULL,
  `email_address` varchar(200) DEFAULT NULL,
  `phone_number` varchar(200) DEFAULT NULL,
  `mobile_number` varchar(200) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `lease_detail` text,
  `lease_ststus` int(2) NOT NULL DEFAULT '1' COMMENT '1=>New, 2=>Read',
  `lease_date` datetime DEFAULT NULL,
  `type_id` int(2) NOT NULL DEFAULT '1' COMMENT '1=>leasing, 2=>lamosen',
  PRIMARY KEY (`lease_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rs_tbl_car_lease`
--

INSERT INTO `rs_tbl_car_lease` (`lease_id`, `lease_type`, `full_name`, `email_address`, `phone_number`, `mobile_number`, `address`, `lease_detail`, `lease_ststus`, `lease_date`, `type_id`) VALUES
(1, 1, 'Numan', 'numan@testing.com', '35152173', '35152173', 'Testing Address', 'asd as das ds dsa das dsa d ads asd as das ds dsa das dsa d ads asd as das ds dsa das dsa d ads asd as das ds dsa das dsa d ads asd as das ds dsa das dsa d ads asd as das ds dsa das dsa d ads asd as das ds dsa das dsa d ads asd as das ds dsa das dsa d ads asd as das ds dsa das dsa d ads asd as das ds dsa das dsa d ads asd as das ds dsa das dsa d ads ', 1, NULL, 1),
(2, 1, 'Numan Tahir', 'numantahir1@gmail.com', '0097335152173', '35152173', 'testing address', 'asd as sa sa as sad asd asd asdsa asd as sa sa as sad asd asd asdsa asd as sa sa as sad asd asd asdsa asd as sa sa as sad asd asd asdsa asd as sa sa as sad asd asd asdsa asd as sa sa as sad asd asd asdsa asd as sa sa as sad asd asd asdsa asd as sa sa as sad asd asd asdsa asd as sa sa as sad asd asd asdsa asd as sa sa as sad asd asd asdsa', 1, '2016-09-08 13:57:26', 1),
(3, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2016-09-08 08:49:32', 1),
(4, 1, 'Asim', 'asimmafzal@yahoo.com', '5555555', '397772224', 'hhhhh', '88888888888888888kkkkkkkkk', 1, '2016-10-03 05:01:48', 1),
(5, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2016-10-24 02:59:41', 2),
(6, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2016-10-27 09:53:39', 2);

-- --------------------------------------------------------

--
-- Table structure for table `rs_tbl_category`
--

CREATE TABLE IF NOT EXISTS `rs_tbl_category` (
  `category_id` int(10) NOT NULL,
  `parent_id` int(10) NOT NULL DEFAULT '0',
  `category_name` varchar(200) NOT NULL,
  `category_status` smallint(1) NOT NULL DEFAULT '1',
  `url_key` varchar(255) NOT NULL,
  `category_counter` int(11) DEFAULT '0',
  `category_image` varchar(200) DEFAULT NULL,
  `cat_lang` varchar(10) DEFAULT NULL,
  `admin_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rs_tbl_category`
--

INSERT INTO `rs_tbl_category` (`category_id`, `parent_id`, `category_name`, `category_status`, `url_key`, `category_counter`, `category_image`, `cat_lang`, `admin_id`) VALUES
(1, 0, 'A - Class', 1, 'a-class', 0, NULL, 'EN', 0),
(2, 0, 'B - Class', 1, 'b-class', 0, NULL, 'EN', 0),
(3, 0, 'C - Class', 1, 'c-class', 0, NULL, 'EN', 0),
(4, 0, 'D - Class', 1, 'd-class', 0, NULL, 'EN', 0),
(5, 0, 'VIP', 1, 'vip', 0, NULL, 'EN', 0),
(6, 0, 'L - Class', 1, 'l-class', 0, NULL, 'EN', 0),
(7, 0, 'E - Class', 1, 'e-class', 0, NULL, 'EN', 0),
(8, 0, 'H - Class', 1, 'h-class', 0, NULL, 'EN', 0);

-- --------------------------------------------------------

--
-- Table structure for table `rs_tbl_config`
--

CREATE TABLE IF NOT EXISTS `rs_tbl_config` (
  `config_id` int(10) NOT NULL,
  `config_category` varchar(100) NOT NULL DEFAULT '''site''',
  `config_caption` varchar(255) NOT NULL,
  `config_key` varchar(255) NOT NULL,
  `config_value` text NOT NULL,
  `config_order` smallint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`config_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rs_tbl_config`
--

INSERT INTO `rs_tbl_config` (`config_id`, `config_category`, `config_caption`, `config_key`, `config_value`, `config_order`) VALUES
(1, 'mail', 'Site E-mail', 'site_email', 'info@foodbayt.com', 1),
(2, 'site', 'Meta Title', 'meta_title', '|| Trans Auto Rental ||', 2),
(3, 'site', 'Meta Keywords', 'meta_keywords', '|| Trans Auto Rental ||', 3),
(4, 'sote', 'Meta Description', 'meta_desc', '|| Trans Auto Rental ||', 4);

-- --------------------------------------------------------

--
-- Table structure for table `rs_tbl_contact`
--

CREATE TABLE IF NOT EXISTS `rs_tbl_contact` (
  `contact_id` int(11) NOT NULL,
  `contact_email` varchar(100) DEFAULT NULL,
  `contact_name` varchar(100) DEFAULT NULL,
  `contact_category_name` varchar(150) DEFAULT NULL,
  `contact_detail` text,
  `contact_date` date DEFAULT NULL,
  `is_active` int(2) DEFAULT '1',
  PRIMARY KEY (`contact_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rs_tbl_contact`
--

INSERT INTO `rs_tbl_contact` (`contact_id`, `contact_email`, `contact_name`, `contact_category_name`, `contact_detail`, `contact_date`, `is_active`) VALUES
(1, 'info@foodbayt.com', 'Food Bayt', 'Administrator', '', '2016-05-03', 1);

-- --------------------------------------------------------

--
-- Table structure for table `rs_tbl_content`
--

CREATE TABLE IF NOT EXISTS `rs_tbl_content` (
  `cms_id` int(11) NOT NULL,
  `cms_title` varchar(255) DEFAULT NULL,
  `cms_detail` text,
  `parent_id` int(11) DEFAULT NULL,
  `cms_category_id` int(11) DEFAULT NULL COMMENT 'for (help & Faq) 1=>Help, 2=>Faq',
  `cms_type_id` int(11) DEFAULT NULL COMMENT '1=>Content Pages, 2=>Help & FAQ, 3=>GUID, 4=>News',
  `other_type` int(1) DEFAULT NULL COMMENT '1=>Basic Help, 2=>Student, 3=>Employee',
  `cms_file` varchar(255) DEFAULT NULL,
  `cms_date` date DEFAULT NULL,
  `is_active` int(2) DEFAULT '1',
  `url_key` varchar(255) DEFAULT NULL,
  `ans_yes` int(11) DEFAULT '0',
  `ans_no` int(11) DEFAULT '0',
  `cms_lang` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`cms_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rs_tbl_content`
--

INSERT INTO `rs_tbl_content` (`cms_id`, `cms_title`, `cms_detail`, `parent_id`, `cms_category_id`, `cms_type_id`, `other_type`, `cms_file`, `cms_date`, `is_active`, `url_key`, `ans_yes`, `ans_no`, `cms_lang`) VALUES
(1, 'About Us', '<p><img src="/rentacar/mfm/upload/profile_cover_1.jpg" alt="" width="100%" /></p>\r\n<p><img src="/rentacar/mfm/upload/profile_cover_2.jpg" alt="" width="100%" /></p>', 0, 0, 1, NULL, '', NULL, 1, 'about-us', 0, 0, NULL),
(2, 'Privacy Policy', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', 0, 0, 1, NULL, NULL, NULL, 1, 'privacy-policy', 0, 0, NULL),
(3, 'Terms of Use', '<p>Transgulf Car Rental WLL rents to the customer representative whose signature and vehicle description appears on the reverse of this page of rental contract, on terms and conditions stated on both sides of this rental contract which the Customer accepts and agrees to observe.</p>\r\n<p>&nbsp;<strong>1.&nbsp;&nbsp;&nbsp;&nbsp; </strong><strong>Rental Period</strong></p>\r\n<p>This contract is valid for the days stated on this rental contract. It may extend to a maximum of 30 days from the rental date. Failure to intimate the rental will be termed as automatic extension and charged accordingly.</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p><strong>&nbsp;</strong><strong>2.&nbsp;&nbsp;&nbsp;&nbsp; </strong><strong>Rental Extension</strong></p>\r\n<p>If the customer wishes to extend the rental period it is mandatory for the Customer to visit, call e-mail or fax to the rental location seeking for an extension.</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p><strong>&nbsp;</strong><strong>3.&nbsp;&nbsp;&nbsp;&nbsp; </strong><strong>Delivery and Return</strong><strong>&nbsp;</strong></p>\r\n<p>The customer agrees to return the vehicle to the location the vehicle is rented from at the end of the rental period. It is mandatory for the renter to bring in the checkout sheet to be compared with the check in sheet (vehicle condition report).</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p><strong>&nbsp;</strong><strong>4.&nbsp;&nbsp;&nbsp;&nbsp; </strong><strong>Charges</strong></p>\r\n<p><span style="white-space: pre;"><span style="white-space: pre;"> </span></span>a) &nbsp; &nbsp; TRANSGULF reserves the right to charge the renter in advance as per the rate agreed in the rental agreement at the start of <span style="white-space: pre;"> </span>each <span style="white-space: pre;"> </span>rental period.</p>\r\n<p><span style="white-space: pre;"> <span style="white-space: pre;"> </span></span>b) &nbsp; &nbsp; In case of Extension | Renewal of Rental Agreement TRANSGULF reserves the right to charge the Customer in advance.</p>\r\n<p><span style="white-space: pre;"> <span style="white-space: pre;"> </span></span>c) &nbsp; &nbsp; On termination 1 Return of the vehicle TRANSGULF reserves the right to charge the customer for all new damages without a <span style="white-space: pre;"> </span>valid <span style="white-space: pre;"> </span>traffic reports, traffic fines and violations, insurance excess with traffic report even after the renter departed from the <span style="white-space: pre;"> </span>Kingdom of <span style="white-space: pre;"> </span>Bahrain.</p>\r\n<p><span style="white-space: pre;"> </span>d) &nbsp; &nbsp; TRANSGULF reserves the right to use the renter''s credit card for any type of unpaid rental, Charges, traffic fines or damages.</p>\r\n<p>&nbsp;</p>\r\n<p><strong>&nbsp;</strong></p>\r\n<p><strong>&nbsp;</strong><strong>5.&nbsp;&nbsp;&nbsp;&nbsp; </strong><strong>Monthly Rental Agreement</strong></p>\r\n<p><span style="white-space: pre;"><span style="white-space: pre;"> </span></span>a) &nbsp; &nbsp;In case of Pre-termination of monthly rental agreement before the expiry of the first month, TRANSGULF reserves the right <span style="white-space: pre;"> </span>to <span style="white-space: pre;"> </span>charge the renter on weekly rate.<strong>&nbsp;</strong></p>\r\n<p><span style="white-space: pre;"> </span>b) &nbsp; &nbsp; It is mandatory for the renter to visit the rented location monthly and sign the new rental  agreement upon renewal. Failure to do <span style="white-space: pre;"> </span>so TRANSGULF reserves the right to charge the renter for the subsequent month.</p>\r\n<p>&nbsp;</p>\r\n<p><strong>&nbsp;</strong></p>\r\n<p><strong>&nbsp;6.&nbsp;&nbsp;&nbsp;&nbsp; </strong><strong>Use of vehicle.</strong><strong>&nbsp;</strong></p>\r\n<p>The vehicle will be driven by the renter in possession of a valid driving license (recognized license by the Bahrain Traffic Authority).<strong>&nbsp;</strong></p>\r\n<p><span style="white-space: pre;"> </span>a)&nbsp;&nbsp;&nbsp;&nbsp; The vehicle can be driven by an additional driver indicated by the renter subject to the approval of TRANSGULF and a valid <span style="white-space: pre;"> </span>driving license.<strong>&nbsp;</strong></p>\r\n<p><span style="white-space: pre;"> </span>b) &nbsp; &nbsp; The renter is entirely responsible for the respect of the general condition ruling this contract by the driver whom he would <span style="white-space: pre;"> </span>have authorized to drive the rented vehicle.</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p><strong>&nbsp;</strong><strong>7.&nbsp;&nbsp;&nbsp;&nbsp; </strong><strong>Age link and Experience</strong><strong>&nbsp;</strong></p>\r\n<p>Minimum age to rent a vehicle is 26 years old and holding a driving license for at least one year for all groups of vehicle. In case of accident by underage customer or holder of a license less than one year, the compulsory insurance policy deductible excess of BD 750.00 will be paid by the renter.</p>\r\n<p><strong>&nbsp;</strong></p>\r\n<p>&nbsp;</p>\r\n<p><strong>&nbsp;8.&nbsp;&nbsp;&nbsp;&nbsp; </strong><strong>Renters Responsibilities in case of accidents &amp; damages</strong></p>\r\n<p><span style="white-space: pre;"> </span>a)&nbsp;&nbsp;&nbsp;&nbsp; In case of accident with or without a third party. It is the responsibility of the customer to call Traffic Authority (199) to report the <span style="white-space: pre;"> </span>incident and wait at the accident site until the traffic police arrives and issues a traffic report.</p>\r\n<p><span style="white-space: pre;"> </span>b)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  No replacement vehicle will be issued to the renter damages without a valid traffic report.</p>\r\n<p><span style="white-space: pre;"> </span>c) &nbsp; &nbsp; &nbsp;It is mandatory for the renter to obtain a valid traffic report for all damages with or without a third &nbsp; party. Failure to do so <span style="white-space: pre;"> </span>TRANSGULF reserves the right to charge the customer the entire damages  cost.</p>\r\n<p><span style="white-space: pre;"> </span>d) &nbsp; &nbsp; If the renter is not at fault as per the traffic report (BD 6000) paid by the renter to obtain the report  will be reimbursed by <span style="white-space: pre;"> </span>TRANSGULF upon dropping the vehicle for repairs.</p>\r\n<p><span style="white-space: pre;"> </span>e) &nbsp;&nbsp;&nbsp;&nbsp; All known / unknown damages without a third party will be termed as the Customer fault and will be charged for all <span style="white-space: pre;"> </span>damages or the insurance excess in case of a valid traffic report accordingly.</p>\r\n<p><strong>&nbsp;</strong></p>\r\n<p>&nbsp;</p>\r\n<p><strong>&nbsp;9.&nbsp;&nbsp;&nbsp;&nbsp; </strong><strong>Replacement Vehicle</strong></p>\r\n<p>TRANSGULF intent to serve the customer with the same group of vehicle as replacement however in case of emergency or running out of specific models TRANSGULF reserves the right to serve with the available nearest group of vehicle based on availability.</p>\r\n<p><strong>&nbsp;</strong></p>\r\n<p>&nbsp;</p>\r\n<p><strong>&nbsp;10.&nbsp;&nbsp; </strong><strong>Personal Effects Protection</strong></p>\r\n<p>TRANSGULF has no responsibility for any claims for personal belongings left in the vehicle on returning the vehicle &amp; at the close of the rental agreement.</p>\r\n<p><strong>&nbsp;</strong></p>\r\n<p>&nbsp;</p>\r\n<p><strong>&nbsp;11.&nbsp;&nbsp; </strong><strong>Use of Fuel</strong></p>\r\n<p>TRANSGULF reserves the right to charge the renter the difference in the fuel level as per the TRANSGULF tariff rates. Upon returning the vehicle, the renter requires to bring in the vehicle with the same level as at the time of renting the vehicle. The renter must make a routine check of all fluid levels in the engine compartment of the rental vehicle. The vehicle should not be with any red Color warning light on of the temperature gauge reads high on the dash board, in such cases shut off the engines immediately and call Our Breakdown assistant on 36899793/17329303 and do not operate the vehicle until the cause is corrected.</p>\r\n<p>Use only MUMTAZ FUEL, failing to which, TRANSGULF reserves the right to charge all damages to the engine and vehicle for using improper rated fuel as per the manufacturer.</p>\r\n<p><strong>&nbsp;&nbsp;</strong></p>\r\n<p>&nbsp;</p>\r\n<p><strong>&nbsp;12.&nbsp;&nbsp; </strong><strong>Traffic Offence</strong></p>\r\n<p>Traffic Offence The renter is responsible for any traffic during the rental period; unpaid traffic offences will be charged to the renter credit cards without prior approval or transferred to the respective license.</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p><strong>&nbsp;13.&nbsp;&nbsp; </strong><strong>Breakdown Service</strong></p>\r\n<p>Breakdown service will be provided within 2 hour from time of call (Subject to the normal traffic flow).</p>\r\n<p>Service available from 06:00 am to 11:00 pm</p>\r\n<p><strong>&nbsp;</strong></p>\r\n<p><strong>&nbsp;</strong><strong>&nbsp;</strong></p>\r\n<p><strong>&nbsp;14.&nbsp;&nbsp; </strong><strong>All Disputes are Subject to the Kingdom of Bahrain Jurisdiction</strong></p>\r\n<p><strong>&nbsp;</strong></p>\r\n<p>&nbsp;</p>\r\n<p><strong>&nbsp;15.&nbsp;&nbsp; </strong><strong>Declaration</strong></p>\r\n<p>Declaration If the renter is at fault as mentioned in the traffic report, the renter is liable to pay BD, 16,000 towards traffic report and BD........ towards insurance excess. TRANSGULF reserves the right to change the insurance excess and for all the other damages not mentioned in the report,</p>', 0, 0, 1, NULL, '', NULL, 1, 'terms-of-use', 0, 0, NULL),
(4, 'Thank You', '<div id="static" class="tab-pane active">\r\n<p><strong style="margin: 0px; padding: 0px; color: #000000; font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; line-height: 20px; orphans: 2; text-align: justify; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; background-color: #ffffff;">Lorem Ipsum</strong><span style="color: #000000; font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; line-height: 20px; orphans: 2; text-align: justify; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; display: inline !important; float: none; background-color: #ffffff;"><span class="Apple-converted-space">&nbsp;</span>is  simply dummy text of the printing and typesetting industry. Lorem Ipsum  has been the industry''s standard dummy text ever since the 1500s, when  an unknown printer took a galley of type and scrambled it to make a type  specimen book. It has survived not only five centuries, but also the  leap into electronic typesetting, remaining essentially unchanged. It  was popularised in the 1960s with the release of Letraset sheets  containing Lorem Ipsum passages, and more recently with desktop  publishing software like Aldus PageMaker including versions of Lorem  Ipsum.</span></p>\r\n</div>', 0, 0, 1, NULL, NULL, NULL, 1, 'thank-you', 0, 0, NULL),
(5, 'Leasing', '<p>&nbsp;</p>\r\n<p>Dummy Text</p>', 0, 0, 1, NULL, '', NULL, 1, 'leasing', 0, 0, NULL),
(6, 'Who we are', '<p><strong style="margin: 0px; padding: 0px; color: #000000; font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; line-height: 20px; orphans: 2; text-align: justify; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; background-color: #ffffff;">Lorem Ipsum</strong><span style="color: #000000; font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; line-height: 20px; orphans: 2; text-align: justify; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; display: inline !important; float: none; background-color: #ffffff;"><span class="Apple-converted-space">&nbsp;</span>is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</span></p>', 0, 0, 1, NULL, NULL, NULL, 1, 'who-we-are', 0, 0, NULL),
(7, 'Contact Us', '<p><span style="text-decoration: underline;"><strong>Main Office</strong></span><br /><strong>Address:</strong> FLAT NO.78, BUILDING NO.1033,ROAD NO.306, BLOCK 327, AL ADLIYA.<br /><strong>Phone:</strong> 17230815<br /><br /><span style="text-decoration: underline;"><strong>Branches:</strong></span><br /><strong>Address:</strong> Sh.Rashid Comm.Centre Office 484,Road 1518,Manama 315. MANAMA<br /><strong>Phone:</strong> 17212100</p>\r\n<hr />\r\n<p><strong>Address:&nbsp;</strong> Airport Arrival, Unit 1257B, Level 0, Bahrain International Airport,&nbsp; Muharraq, Bahrain.(Airport 24 hrs)<br /><strong>Phone:</strong> 17329303</p>\r\n<hr />\r\n<p><strong>Address:</strong> Shop No.211A, Building No.459D,Road No.2013,Street 18,Hoora 320, Exhibition road,<br /><strong>Phone:</strong> 17294000<br />\r\n<hr />\r\n<strong>Address:</strong> Deal Garage Building -2220,Road-704,Block-437, Salmabad.<br /><strong>Phone:</strong> 17641221</p>\r\n<p>&nbsp;</p>', 0, 0, 1, NULL, '', NULL, 1, 'contact-us', 0, 0, NULL),
(8, 'Limousine', '<p><img src="/rentacar/mfm/upload/1.jpg" alt="" width="560" height="315" /></p>', 0, 0, 1, NULL, '', NULL, 1, 'limousine', 0, 0, NULL),
(9, 'Special Offer', '<table border="0" cellspacing="5" cellpadding="5" width="100%">\r\n<tbody>\r\n<tr>\r\n<td><img src="/rentacar/mfm/upload/sp-1.jpg" alt="" width="570" height="230" /></td>\r\n<td><img src="/rentacar/mfm/upload/sp-2.jpg" alt="" width="570" height="230" /></td>\r\n</tr>\r\n<tr>\r\n<td><img src="/rentacar/mfm/upload/sp-3.jpg" alt="" width="570" height="230" /></td>\r\n<td><img src="/rentacar/mfm/upload/sp-4.jpg" alt="" width="570" height="230" /></td>\r\n</tr>\r\n<tr>\r\n<td><img src="/rentacar/mfm/upload/sp-5.jpg" alt="" width="570" height="230" /></td>\r\n<td><img src="/rentacar/mfm/upload/sp-6.jpg" alt="" width="570" height="230" /></td>\r\n</tr>\r\n</tbody>\r\n</table>', 0, 0, 1, NULL, '', NULL, 1, 'special-offer', 0, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `rs_tbl_countries`
--

CREATE TABLE IF NOT EXISTS `rs_tbl_countries` (
  `country_id` int(11) NOT NULL AUTO_INCREMENT,
  `country_name` varchar(64) NOT NULL DEFAULT '',
  `iso_code_2` char(2) NOT NULL,
  `iso_code_3` char(3) NOT NULL,
  `format_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`country_id`),
  KEY `IDX_COUNTRY_NAME` (`country_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=240 ;

--
-- Dumping data for table `rs_tbl_countries`
--

INSERT INTO `rs_tbl_countries` (`country_id`, `country_name`, `iso_code_2`, `iso_code_3`, `format_id`) VALUES
(1, 'Afghanistan', 'AF', 'AFG', 1),
(2, 'Albania', 'AL', 'ALB', 1),
(3, 'Algeria', 'DZ', 'DZA', 1),
(4, 'American Samoa', 'AS', 'ASM', 1),
(5, 'Andorra', 'AD', 'AND', 1),
(6, 'Angola', 'AO', 'AGO', 1),
(7, 'Anguilla', 'AI', 'AIA', 1),
(8, 'Antarctica', 'AQ', 'ATA', 1),
(9, 'Antigua and Barbuda', 'AG', 'ATG', 1),
(10, 'Argentina', 'AR', 'ARG', 1),
(11, 'Armenia', 'AM', 'ARM', 1),
(12, 'Aruba', 'AW', 'ABW', 1),
(13, 'Australia', 'AU', 'AUS', 5),
(14, 'Austria', 'AT', 'AUT', 1),
(15, 'Azerbaijan', 'A', 'AZE', 1),
(16, 'Bahamas', 'BS', 'BHS', 1),
(17, 'Bahrain', 'BH', 'BHR', 1),
(18, 'Bangladesh', 'BD', 'B', 1),
(19, 'Barbados', 'BB', 'BRB', 1),
(20, 'Belarus', 'BY', 'BLR', 1),
(21, 'Belgium', 'BE', 'BEL', 1),
(22, 'Belize', 'BZ', 'BLZ', 1),
(23, 'Benin', 'BJ', 'BEN', 1),
(24, 'Bermuda', 'BM', 'BMU', 1),
(25, 'Bhutan', 'BT', 'BTN', 1),
(26, 'Bolivia', 'BO', 'BOL', 1),
(27, 'Bosnia and Herzegowina', 'BA', 'BIH', 1),
(28, 'Botswana', 'BW', 'BWA', 1),
(29, 'Bouvet Island', 'BV', 'BVT', 1),
(30, 'Brazil', 'BR', 'BRA', 1),
(31, 'British Indian Ocean Territory', 'IO', 'IOT', 1),
(32, 'Brunei Darussalam', 'BN', 'BRN', 1),
(33, 'Bulgaria', 'BG', 'BGR', 1),
(34, 'Burkina Faso', 'BF', 'BFA', 1),
(35, 'Burundi', 'BI', 'BDI', 1),
(36, 'Cambodia', 'KH', 'KHM', 1),
(37, 'Cameroon', 'CM', 'CMR', 1),
(38, 'Canada', 'CA', 'CAN', 1),
(39, 'Cape Verde', 'CV', 'CPV', 1),
(40, 'Cayman Islands', 'KY', 'CYM', 1),
(41, 'Central African Republic', 'CF', 'CAF', 1),
(42, 'Chad', 'TD', 'TCD', 1),
(43, 'Chile', 'CL', 'CHL', 1),
(44, 'China', 'CN', 'CHN', 1),
(45, 'Christmas Island', 'CX', 'CXR', 1),
(46, 'Cocos (Keeling) Islands', 'CC', 'CCK', 1),
(47, 'Colombia', 'CO', 'COL', 1),
(48, 'Comoros', 'KM', 'COM', 1),
(49, 'Congo', 'CG', 'COG', 1),
(50, 'Cook Islands', 'CK', 'COK', 1),
(51, 'Costa Rica', 'CR', 'CRI', 1),
(52, 'CoteIvoire', 'CI', 'CIV', 1),
(53, 'Croatia', 'HR', 'HRV', 1),
(54, 'Cuba', 'CU', 'CUB', 1),
(55, 'Cyprus', '', 'CYP', 1),
(56, 'Czech Republic', 'CZ', 'CZE', 1),
(57, 'Denmark', 'DK', 'DNK', 1),
(58, 'Djibouti', 'DJ', 'DJI', 1),
(59, 'Dominica', 'DM', 'DMA', 1),
(60, 'Dominican Republic', 'DO', 'DOM', 1),
(61, 'East Timor', 'TP', 'TMP', 1),
(62, 'Ecuador', 'EC', 'ECU', 1),
(63, 'Egypt', 'EG', 'EGY', 1),
(64, 'El Salvador', 'SV', 'SLV', 1),
(65, 'Equatorial Guinea', 'GQ', 'GNQ', 1),
(66, 'Eritrea', 'ER', 'ERI', 1),
(67, 'Estonia', 'EE', 'EST', 1),
(68, 'Ethiopia', 'ET', 'ETH', 1),
(69, 'Falkland Islands (Malvinas)', 'FK', 'FLK', 1),
(70, 'Faroe Islands', 'FO', 'FRO', 1),
(71, 'Fiji', 'FJ', 'FJI', 1),
(72, 'Finland', 'FI', 'FIN', 1),
(73, 'France', 'FR', 'FRA', 1),
(74, 'France, Metropolitan', 'FX', 'FXX', 1),
(75, 'French Guiana', 'GF', 'GUF', 1),
(76, 'French Polynesia', 'PF', 'PYF', 1),
(77, 'French Southern Territories', 'TF', 'ATF', 1),
(78, 'Gabon', 'GA', 'GAB', 1),
(79, 'Gambia', 'GM', 'GMB', 1),
(80, 'Georgia', 'GE', 'GEO', 1),
(81, 'Germany', 'DE', 'DEU', 1),
(82, 'Ghana', 'GH', 'GHA', 1),
(83, 'Gibraltar', 'GI', '', 1),
(84, 'Greece', 'GR', 'GRC', 1),
(85, 'Greenland', 'GL', 'GRL', 1),
(86, 'Grenada', 'GD', 'GRD', 1),
(87, 'Guadeloupe', 'GP', 'GLP', 1),
(88, 'Guam', 'GU', 'GUM', 1),
(89, 'Guatemala', 'GT', 'GTM', 1),
(90, 'Guinea', 'GN', 'GIN', 1),
(91, 'Guinea-bissau', 'GW', 'GNB', 1),
(92, 'Guyana', 'GY', 'GUY', 1),
(93, 'Haiti', 'HT', 'HTI', 1),
(94, 'Heard and Mc Donald Islands', 'HM', 'HMD', 1),
(95, 'Honduras', 'HN', 'HND', 1),
(96, 'Hong Kong', 'HK', 'HKG', 1),
(97, 'Hungary', 'HU', 'HUN', 1),
(98, 'Iceland', 'IS', 'ISL', 1),
(99, 'India', 'IN', 'IND', 1),
(100, 'Indonesia', 'ID', 'IDN', 1),
(101, 'Iran (Islamic Republic of)', 'IR', 'IRN', 1),
(102, 'Iraq', 'IQ', 'IRQ', 1),
(103, 'Ireland', 'IE', 'IRL', 1),
(104, 'Israel', 'IL', 'ISR', 1),
(105, 'Italy', 'IT', 'ITA', 1),
(106, 'Jamaica', 'JM', 'JAM', 1),
(107, 'Japan', 'JP', 'JPN', 1),
(108, 'Jordan', 'JO', 'JOR', 1),
(109, 'Kazakhstan', 'KZ', 'KAZ', 1),
(110, 'Kenya', 'KE', 'KEN', 1),
(111, 'Kiribati', 'KI', 'KIR', 1),
(112, 'Korea, Democratic Republic', 'KP', 'PRK', 1),
(113, 'Korea, Republic', 'KR', 'KOR', 1),
(114, 'Kuwait', 'KW', 'KWT', 1),
(115, 'Kyrgyzstan', 'KG', 'KGZ', 1),
(116, 'Lao Peoples Democratic Republic', 'LA', 'LAO', 1),
(117, 'Latvia', 'LV', 'LVA', 1),
(118, 'Lebanon', 'LB', 'LBN', 1),
(119, 'Lesotho', 'LS', 'LSO', 1),
(120, 'Liberia', 'LR', 'LBR', 1),
(121, 'Libyan Arab Jamahiriya', 'LY', 'LBY', 1),
(122, 'Liechtenstein', 'LI', 'LIE', 1),
(123, 'Lithuania', 'LT', 'LTU', 1),
(124, 'Luxembourg', 'LU', 'LUX', 1),
(125, 'Macau', 'MO', 'MAC', 1),
(126, 'Macedonia', 'MK', 'MKD', 1),
(127, 'Madagascar', 'MG', 'MDG', 1),
(128, 'Malawi', 'MW', 'MWI', 1),
(129, 'Malaysia', 'MY', 'MYS', 1),
(130, 'Maldives', 'MV', 'MDV', 1),
(131, 'Mali', 'ML', 'MLI', 1),
(132, 'Malta', 'MT', 'M', 1),
(133, 'Marshall Islands', 'MH', 'MHL', 1),
(134, 'Martinique', 'MQ', 'MTQ', 1),
(135, 'Mauritania', 'MR', 'MRT', 1),
(136, 'Mauritius', 'MU', 'MUS', 1),
(137, 'Mayotte', 'YT', 'MYT', 1),
(138, 'Mexico', 'MX', 'MEX', 1),
(139, 'Micronesia', 'FM', 'FSM', 1),
(140, 'Moldova', 'MD', 'MDA', 1),
(141, 'Monaco', 'MC', 'MCO', 1),
(142, 'Mongolia', 'MN', 'MNG', 1),
(143, 'Montserrat', 'MS', 'MSR', 1),
(144, 'Morocco', 'MA', 'MAR', 1),
(145, 'Mozambique', 'MZ', 'MOZ', 1),
(146, 'Myanmar', 'MM', 'MMR', 1),
(147, 'Namibia', 'NA', 'NAM', 1),
(148, 'Nauru', 'NR', 'NRU', 1),
(149, 'Nepal', 'NP', 'NPL', 1),
(150, 'Netherlands', 'NL', 'NLD', 1),
(151, 'Netherlands Antilles', 'AN', 'ANT', 1),
(152, 'New Caledonia', 'NC', 'NCL', 1),
(153, 'New Zealand', 'NZ', 'NZL', 1),
(154, 'Nicaragua', 'NI', 'NIC', 1),
(155, 'Niger', 'NE', 'NER', 1),
(156, 'Nigeria', 'NG', 'NGA', 1),
(157, 'Niue', 'NU', 'NIU', 1),
(158, 'Norfolk Island', 'NF', 'NFK', 1),
(159, 'Northern Mariana Islands', 'MP', 'MNP', 1),
(160, 'Norway', 'NO', 'NOR', 1),
(161, 'Oman', 'OM', 'OMN', 1),
(162, 'Pakistan', 'PK', 'PAK', 1),
(163, 'Palau', 'PW', 'PLW', 1),
(164, 'Panama', 'PA', 'PAN', 1),
(165, 'Papua New Guinea', 'PG', 'PNG', 1),
(166, 'Paraguay', 'PY', 'PRY', 1),
(167, 'Peru', 'PE', 'PER', 1),
(168, 'Philippines', 'PH', 'PHL', 1),
(169, 'Pitcairn', 'PN', 'PCN', 1),
(170, 'Poland', 'PL', 'POL', 1),
(171, 'Portugal', 'PT', 'PRT', 1),
(172, 'Puerto Rico', 'PR', 'PRI', 1),
(173, 'Qatar', 'QA', 'QAT', 1),
(174, 'Reunion', 'RE', 'REU', 1),
(175, 'Romania', '', 'ROM', 1),
(176, 'Russian Federation', 'RU', 'RUS', 1),
(177, 'Rwanda', 'RW', 'RWA', 1),
(178, 'Saint Kitts and Nevis', 'KN', 'KNA', 1),
(179, 'Saint Lucia', 'LC', 'LCA', 1),
(180, 'Saint Vincent and the Grenadines', 'VC', 'VCT', 1),
(181, 'Samoa', 'WS', 'WSM', 1),
(182, 'San Marino', 'SM', 'SMR', 1),
(183, 'Sao Tome and Principe', 'ST', 'STP', 1),
(184, 'Saudi Arabia', 'SA', 'SAU', 1),
(185, 'Senegal', 'SN', 'SEN', 1),
(186, 'Seychelles', 'SC', 'SYC', 1),
(187, 'Sierra Leone', 'SL', 'SLE', 1),
(188, 'Singapore', 'SG', 'SGP', 1),
(189, 'Slovakia (Slovak Republic)', 'SK', 'SVK', 1),
(190, 'Slovenia', 'SI', 'SVN', 1),
(191, 'Solomon Islands', 'SB', 'SLB', 1),
(192, 'Somalia', 'SO', 'SOM', 1),
(193, 'South Africa', 'ZA', 'ZAF', 1),
(194, 'South Georgia and the South Sandwich Islands', 'GS', 'SGS', 1),
(195, 'Spain', 'ES', 'ESP', 1),
(196, 'Sri Lanka', 'LK', 'LKA', 1),
(197, 'St. Helena', 'SH', 'SHN', 1),
(198, 'St. Pierre and Miquelon', 'PM', 'SPM', 1),
(199, 'Sudan', 'SD', 'SDN', 1),
(200, 'Suriname', 'SR', 'SUR', 1),
(201, 'Svalbard and Jan Mayen Islands', 'SJ', 'SJM', 1),
(202, 'Swaziland', 'SZ', 'SWZ', 1),
(203, 'Sweden', 'SE', 'SWE', 1),
(204, 'Switzerland', 'CH', 'CHE', 1),
(205, 'Syrian Arab Republic', 'SY', 'SYR', 1),
(206, 'Taiwan', 'TW', 'TWN', 1),
(207, 'Tajikistan', 'TJ', 'TJK', 1),
(208, 'Tanzania, United Republic of', 'TZ', 'TZA', 1),
(209, 'Thailand', 'TH', 'THA', 1),
(210, 'Togo', 'TG', 'TGO', 1),
(211, 'Tokelau', 'TK', 'TKL', 1),
(212, 'Tonga', 'TO', 'TON', 1),
(213, 'Trinidad and Tobago', 'TT', 'TTO', 1),
(214, 'Tunisia', 'TN', 'TUN', 1),
(215, 'Turkey', 'TR', 'TUR', 1),
(216, 'Turkmenistan', 'TM', 'TKM', 1),
(217, 'Turks and Caicos Islands', 'TC', 'TCA', 1),
(218, 'Tuvalu', 'TV', 'TUV', 1),
(219, 'Uganda', 'UG', 'UGA', 1),
(220, 'Ukraine', 'UA', 'UKR', 1),
(221, 'United Arab Emirates', 'AE', 'ARE', 1),
(222, 'United Kingdom', 'GB', 'GBR', 1),
(223, 'United States', 'US', 'USA', 1),
(224, 'United States Minor Outlying Islands', 'UM', 'UMI', 1),
(225, 'Uruguay', 'UY', 'URY', 1),
(226, 'Uzbekistan', 'UZ', 'UZB', 1),
(227, 'Vanuatu', 'VU', 'VUT', 1),
(228, 'Vatican City State (Holy See)', 'VA', 'VAT', 1),
(229, 'Venezuela', 'VE', 'VEN', 1),
(230, 'Viet Nam', 'VN', 'VNM', 1),
(231, 'Virgin Islands (British)', 'VG', 'VGB', 1),
(232, 'Virgin Islands (U.S.)', 'VI', 'VIR', 1),
(233, 'Wallis and Futuna Islands', 'WF', 'WLF', 1),
(234, 'Western Sahara', 'EH', 'ESH', 1),
(235, 'Yemen', 'YE', 'YEM', 1),
(236, 'Yugoslavia', 'YU', 'YUG', 1),
(237, 'Zaire', 'ZR', 'ZAR', 1),
(238, 'Zambia', 'ZM', 'ZMB', 1),
(239, 'Zimbabwe', 'ZW', 'ZWE', 1);

-- --------------------------------------------------------

--
-- Table structure for table `rs_tbl_customer`
--

CREATE TABLE IF NOT EXISTS `rs_tbl_customer` (
  `customer_id` int(10) NOT NULL,
  `email` varchar(255) NOT NULL,
  `pass` varchar(32) NOT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `gender` int(2) DEFAULT '1' COMMENT '1=>Male, 2=>Female',
  `dob` date DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `zip_code` varchar(50) DEFAULT NULL,
  `country` varchar(10) DEFAULT NULL,
  `phone` varchar(30) DEFAULT NULL,
  `mobile` varchar(30) DEFAULT NULL,
  `reg_date` datetime DEFAULT NULL,
  `is_active` smallint(4) NOT NULL DEFAULT '0' COMMENT '1=>Active, 2=>InActive, 3=>Payment Pending, 4=>Expire',
  `customer_type` int(2) DEFAULT '1' COMMENT '1=>Student, 2=>Business, 3=>University, 4=>Recruiters',
  `profile_image` varchar(255) DEFAULT NULL,
  `sec_question` int(5) DEFAULT NULL,
  `sec_answer` text,
  `url_key` varchar(255) DEFAULT NULL,
  `package_id` int(11) DEFAULT NULL,
  `about_us` text,
  `guide_status` int(11) NOT NULL DEFAULT '0',
  `driver_license` int(2) DEFAULT NULL COMMENT '1=>Automatic, 2=>Manual',
  `vehicle_status` int(3) DEFAULT '3' COMMENT '1=>Yes, 2=>No, 3=>Not applicable',
  `smking_status` int(3) DEFAULT '3' COMMENT '1=>Yes, 2=>No, 3=>Not applicable',
  `industry_id` int(11) DEFAULT '0',
  `position_id` int(11) DEFAULT '0',
  PRIMARY KEY (`customer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rs_tbl_customer`
--

INSERT INTO `rs_tbl_customer` (`customer_id`, `email`, `pass`, `first_name`, `last_name`, `gender`, `dob`, `address`, `city`, `state`, `zip_code`, `country`, `phone`, `mobile`, `reg_date`, `is_active`, `customer_type`, `profile_image`, `sec_question`, `sec_answer`, `url_key`, `package_id`, `about_us`, `guide_status`, `driver_license`, `vehicle_status`, `smking_status`, `industry_id`, `position_id`) VALUES
(1, 'numan@testing.com', 'ellight', 'Numan', 'Tahir', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 3, 3, 0, 0),
(2, 'asmaghafoor786@gmail.com', '4518bda19425be276817a3b3eeee82b8', 'Namozaj', 'Namozaj', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 3, 3, 0, 0),
(3, 'asmaghafoor786@gmail.com', '4518bda19425be276817a3b3eeee82b8', 'Namozaj', 'Namozaj', NULL, NULL, NULL, NULL, NULL, NULL, 'BH', NULL, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 3, 3, 0, 0),
(4, 'asmaghafoor786@gmail.com', '4518bda19425be276817a3b3eeee82b8', 'Namozaj', 'Namozaj', NULL, NULL, NULL, NULL, NULL, NULL, 'BH', NULL, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 3, 3, 0, 0),
(5, 'asmaghafoor786@gmail.com', '4518bda19425be276817a3b3eeee82b8', 'Namozaj', 'Namozaj', NULL, NULL, NULL, NULL, NULL, NULL, 'BH', NULL, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 3, 3, 0, 0),
(6, 'asmaghafoor786@gmail.com', '4518bda19425be276817a3b3eeee82b8', 'Namozaj', 'Namozaj', NULL, NULL, NULL, NULL, NULL, NULL, 'BH', NULL, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 3, 3, 0, 0),
(7, 'asmaghafoor786@gmail.com', '4518bda19425be276817a3b3eeee82b8', 'Namozaj', 'Namozaj', NULL, NULL, NULL, NULL, NULL, NULL, 'BH', NULL, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 3, 3, 0, 0),
(8, 'asmaghafoor786@gmail.com', '4518bda19425be276817a3b3eeee82b8', 'Namozaj', 'Namozaj', NULL, NULL, NULL, NULL, NULL, NULL, 'BH', NULL, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 3, 3, 0, 0),
(9, 'asmaghafoor786@gmail.com', '4518bda19425be276817a3b3eeee82b8', 'Namozaj', 'Namozaj', NULL, NULL, NULL, NULL, NULL, NULL, 'BH', NULL, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 3, 3, 0, 0),
(10, 'asmaghafoor786@gmail.com', '4518bda19425be276817a3b3eeee82b8', 'Namozaj', 'Namozaj', NULL, NULL, NULL, NULL, NULL, NULL, 'BH', NULL, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 3, 3, 0, 0),
(11, 'asmaghafoor786@gmail.com', '4518bda19425be276817a3b3eeee82b8', 'Namozaj', 'Namozaj', NULL, NULL, NULL, NULL, NULL, NULL, 'BH', NULL, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 3, 3, 0, 0),
(12, 'numantahir1@gmail.com', '4518bda19425be276817a3b3eeee82b8', 'Numan', 'Tahir', NULL, NULL, NULL, NULL, NULL, NULL, 'BH', NULL, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 3, 3, 0, 0),
(13, 'ajith@transautorental.com', '4518bda19425be276817a3b3eeee82b8', 'Ajith', 'Kumar', NULL, NULL, NULL, NULL, NULL, NULL, 'BH', '38380449', NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 3, 3, 0, 0),
(14, 'ajith.vasudevannair@gmail.com', '4518bda19425be276817a3b3eeee82b8', 'Ajith', 'Kumar', NULL, NULL, NULL, NULL, NULL, NULL, 'BH', '388889999', NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 3, 3, 0, 0),
(15, 'ajith.vasudevannair@gmail.com', '4518bda19425be276817a3b3eeee82b8', 'Asim', 'Afzal', NULL, NULL, NULL, NULL, NULL, NULL, 'BH', '39777777', NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 3, 3, 0, 0),
(16, 'ajith@transautorental.com', '4518bda19425be276817a3b3eeee82b8', 'Ajith', 'ajith', NULL, NULL, NULL, NULL, NULL, NULL, 'BH', '38448407', NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 3, 3, 0, 0),
(17, 'ajith@transautorental.com', '4518bda19425be276817a3b3eeee82b8', 'Ajith', 'Kumar', NULL, NULL, NULL, NULL, NULL, NULL, 'BH', '38380449', NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 3, 3, 0, 0),
(18, 'ajith@transautorental.com', '4518bda19425be276817a3b3eeee82b8', 'Ajith', 'Ajith', NULL, NULL, NULL, NULL, NULL, NULL, 'BH', '38380449', NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 3, 3, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `rs_tbl_etemplates`
--

CREATE TABLE IF NOT EXISTS `rs_tbl_etemplates` (
  `template_id` int(11) NOT NULL,
  `template_title` varchar(255) NOT NULL,
  `template_subject` text NOT NULL,
  `template_detail` text NOT NULL,
  `sender_name` varchar(255) NOT NULL,
  `sender_email` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`template_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `rs_tbl_extra_features`
--

CREATE TABLE IF NOT EXISTS `rs_tbl_extra_features` (
  `extra_feature_id` int(11) NOT NULL,
  `feature_name` varchar(255) DEFAULT NULL,
  `feature_detail` text,
  `feature_icon` varchar(10) DEFAULT NULL,
  `feature_price` varchar(5) DEFAULT NULL,
  `is_active` int(2) NOT NULL DEFAULT '1' COMMENT '1=>Active, 2=>InActive',
  PRIMARY KEY (`extra_feature_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rs_tbl_extra_features`
--

INSERT INTO `rs_tbl_extra_features` (`extra_feature_id`, `feature_name`, `feature_detail`, `feature_icon`, `feature_price`, `is_active`) VALUES
(1, 'Feature 1', '<p>asd asdadas das dsa das das ds ad sads</p>', 'ebbe7.jpg', '3', 1);

-- --------------------------------------------------------

--
-- Table structure for table `rs_tbl_faq`
--

CREATE TABLE IF NOT EXISTS `rs_tbl_faq` (
  `faq_id` int(11) NOT NULL,
  `faq_title` varchar(255) DEFAULT NULL,
  `faq_answer` text,
  `faq_date` date DEFAULT '0000-00-00',
  `faq_status` int(2) NOT NULL DEFAULT '1' COMMENT '1=>Active, 2=>InActive',
  PRIMARY KEY (`faq_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rs_tbl_faq`
--

INSERT INTO `rs_tbl_faq` (`faq_id`, `faq_title`, `faq_answer`, `faq_date`, `faq_status`) VALUES
(1, 'FAQ 1', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', '2016-08-30', 1),
(2, 'FAQ 2', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', '2016-08-30', 1),
(3, 'FAQ 3', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', '2016-08-30', 1);

-- --------------------------------------------------------

--
-- Table structure for table `rs_tbl_feedback`
--

CREATE TABLE IF NOT EXISTS `rs_tbl_feedback` (
  `feedback_id` int(11) NOT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `fdb_name` varchar(255) DEFAULT NULL,
  `fdb_email` varchar(255) DEFAULT NULL,
  `fdb_subject` varchar(255) DEFAULT NULL,
  `fdb_message_type` int(4) NOT NULL DEFAULT '1' COMMENT '1=>Comment, 2=>Suggestion, 3=>Complaint, 4=>Request',
  `fdb_message_box` text,
  `fdb_date` date DEFAULT NULL,
  `fdb_status` int(2) DEFAULT '1' COMMENT '1=>New, 2=>Read',
  PRIMARY KEY (`feedback_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rs_tbl_feedback`
--

INSERT INTO `rs_tbl_feedback` (`feedback_id`, `customer_id`, `fdb_name`, `fdb_email`, `fdb_subject`, `fdb_message_type`, `fdb_message_box`, `fdb_date`, `fdb_status`) VALUES
(1, 1, 'Numan', 'numnatahir1@gmail.com', 'TEsting', 1, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `rs_tbl_location`
--

CREATE TABLE IF NOT EXISTS `rs_tbl_location` (
  `location_id` int(11) NOT NULL,
  `location_title` varchar(255) DEFAULT NULL,
  `location_price` varchar(10) DEFAULT NULL,
  `is_active` int(2) NOT NULL DEFAULT '1' COMMENT '1=>Active, 2=>InActive',
  PRIMARY KEY (`location_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rs_tbl_location`
--

INSERT INTO `rs_tbl_location` (`location_id`, `location_title`, `location_price`, `is_active`) VALUES
(1, 'Manama - Shaikh Rashid Building', '0', 1),
(2, 'Airport', '0', 1),
(3, 'Exhibition Road', NULL, 1),
(4, 'Adliya', NULL, 1),
(5, 'Salmabad', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `rs_tbl_order`
--

CREATE TABLE IF NOT EXISTS `rs_tbl_order` (
  `order_id` varchar(30) NOT NULL,
  `customer_id` int(10) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_name` varchar(255) DEFAULT NULL,
  `order_date` datetime DEFAULT NULL,
  `sub_total` double(16,2) DEFAULT NULL,
  `grand_total` double(16,2) DEFAULT NULL,
  `order_type` int(2) NOT NULL DEFAULT '1' COMMENT '1=>Daily, 2=>Monthly',
  `start_date` varchar(100) DEFAULT NULL,
  `end_date` varchar(100) DEFAULT NULL,
  `pickup_location_id` int(10) DEFAULT NULL,
  `drop_location_id` int(10) DEFAULT NULL,
  `product_price` double(16,2) DEFAULT NULL,
  `order_status` int(2) NOT NULL DEFAULT '1' COMMENT '1=>Pending, 2=>Paid',
  `payment_method` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`order_id`),
  UNIQUE KEY `order_id` (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rs_tbl_order`
--

INSERT INTO `rs_tbl_order` (`order_id`, `customer_id`, `product_id`, `product_name`, `order_date`, `sub_total`, `grand_total`, `order_type`, `start_date`, `end_date`, `pickup_location_id`, `drop_location_id`, `product_price`, `order_status`, `payment_method`) VALUES
('3aa37e00016', 16, 3, 'HONDA CIVIC', '2016-10-30 02:59:20', NULL, 76.00, 1, '10/31/2016 10:00', '11/01/2016 10:00', NULL, NULL, 56.00, 1, NULL),
('45871b00015', 15, 1, 'NISSAN MICRA', '2016-10-03 06:21:24', NULL, 162.00, 1, '10/03/2016 10:00', '10/06/2016 10:00', 1, 1, 34.00, 1, NULL),
('48330000014', 14, 1, 'NISSAN MICRA', '2016-10-03 06:02:43', NULL, 108.00, 1, '10/03/2016 13:00', '10/05/2016 13:00', 1, 1, 34.00, 1, NULL),
('48566100012', 12, 1, 'Car 1', '2016-09-08 13:39:45', NULL, 78.00, 1, '09/08/2016 10:00', '09/11/2016 10:00', 1, NULL, 3.00, 1, NULL),
('b4061200013', 13, 1, 'NISSAN MICRA', '2016-10-01 04:39:43', NULL, 448.00, 1, '10/03/2016 10:00', '10/11/2016 10:00', NULL, NULL, 36.00, 1, NULL),
('d4a34600011', 11, 1, 'Car 1', '2016-09-08 13:38:47', NULL, 48.00, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 2, NULL, 3.00, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `rs_tbl_order_details`
--

CREATE TABLE IF NOT EXISTS `rs_tbl_order_details` (
  `order_detail_id` int(10) NOT NULL,
  `order_id` varchar(50) NOT NULL DEFAULT '',
  `customer_id` int(11) NOT NULL,
  `extra_feature_id` int(11) NOT NULL,
  `feature_name` varchar(255) DEFAULT NULL,
  `quantity` int(5) DEFAULT NULL,
  `price` double(10,2) DEFAULT NULL,
  PRIMARY KEY (`order_detail_id`),
  KEY `order_id` (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rs_tbl_order_details`
--

INSERT INTO `rs_tbl_order_details` (`order_detail_id`, `order_id`, `customer_id`, `extra_feature_id`, `feature_name`, `quantity`, `price`) VALUES
(1, 'd4a34600011', 11, 1, 'Feature 1', 1, 3.00),
(2, '48566100012', 12, 1, 'Feature 1', 1, 3.00);

-- --------------------------------------------------------

--
-- Table structure for table `rs_tbl_partner`
--

CREATE TABLE IF NOT EXISTS `rs_tbl_partner` (
  `partner_id` int(11) NOT NULL,
  `partner_name` varchar(255) DEFAULT NULL,
  `partner_detail` text,
  `partner_logo` varchar(200) DEFAULT NULL,
  `partner_url` varchar(255) DEFAULT NULL,
  `is_active` int(2) NOT NULL DEFAULT '1' COMMENT '1=>Active, 2=>InActive',
  PRIMARY KEY (`partner_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rs_tbl_partner`
--

INSERT INTO `rs_tbl_partner` (`partner_id`, `partner_name`, `partner_detail`, `partner_logo`, `partner_url`, `is_active`) VALUES
(1, 'Partner name', '<p>asda sda da d d asdasd</p>', 'ce715-1.jpg', 'www.abc.com', 1);

-- --------------------------------------------------------

--
-- Table structure for table `rs_tbl_products`
--

CREATE TABLE IF NOT EXISTS `rs_tbl_products` (
  `product_id` int(10) NOT NULL,
  `category_id` smallint(5) DEFAULT NULL,
  `subcat_id` smallint(5) DEFAULT NULL,
  `product_name` varchar(250) NOT NULL,
  `product_keyword` mediumtext,
  `product_description` longtext,
  `product_price` varchar(10) DEFAULT NULL,
  `price_weekly` varchar(10) DEFAULT NULL,
  `price_monthly` varchar(10) DEFAULT NULL,
  `discount_price` varchar(10) DEFAULT NULL,
  `product_type` int(3) NOT NULL DEFAULT '1' COMMENT '1=>Daily, 2=>Monthly',
  `product_image` varchar(100) DEFAULT NULL,
  `model_number` varchar(10) DEFAULT NULL,
  `number_of_seats` varchar(10) DEFAULT NULL,
  `number_of_luggage` varchar(10) DEFAULT NULL,
  `number_of_doors` varchar(10) DEFAULT NULL,
  `l_engine` varchar(10) DEFAULT NULL,
  `ac_option` int(2) NOT NULL DEFAULT '1' COMMENT '1=>Yes, 2=>No',
  `gar_type` int(2) NOT NULL DEFAULT '1' COMMENT '1=>Auto, 2=>Manual',
  `set_special_offer` int(2) NOT NULL DEFAULT '2' COMMENT '1=>Special, 2=>Normal',
  `other_f_1` varchar(200) DEFAULT NULL,
  `other_f_2` varchar(200) DEFAULT NULL,
  `other_f_3` varchar(200) DEFAULT NULL,
  `other_f_4` varchar(200) DEFAULT NULL,
  `other_f_5` varchar(200) DEFAULT NULL,
  `other_f_6` varchar(200) DEFAULT NULL,
  `other_f_7` varchar(200) DEFAULT NULL,
  `other_f_8` varchar(200) DEFAULT NULL,
  `other_f_9` varchar(200) DEFAULT NULL,
  `is_active` smallint(4) NOT NULL DEFAULT '1' COMMENT '1=Active, 2=>Pending Approval, 3=>Editting Required, 4=>Delete',
  `url_key` mediumtext,
  `submit_date` datetime DEFAULT NULL,
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rs_tbl_products`
--

INSERT INTO `rs_tbl_products` (`product_id`, `category_id`, `subcat_id`, `product_name`, `product_keyword`, `product_description`, `product_price`, `price_weekly`, `price_monthly`, `discount_price`, `product_type`, `product_image`, `model_number`, `number_of_seats`, `number_of_luggage`, `number_of_doors`, `l_engine`, `ac_option`, `gar_type`, `set_special_offer`, `other_f_1`, `other_f_2`, `other_f_3`, `other_f_4`, `other_f_5`, `other_f_6`, `other_f_7`, `other_f_8`, `other_f_9`, `is_active`, `url_key`, `submit_date`) VALUES
(2, 0, 1, 'NISSAN SUNNY', NULL, '', '10', '60', '180', NULL, 1, '2dac4.jpg', 'Economy', '5', '2', '5', '1.5', 1, 1, 1, 'Cancellation', 'Theft Protection', 'Amendments', 'Collision Damage Waiver', '', '', '', '', '', 1, 'nissan-sunny', '2016-09-26 14:30:29'),
(7, 1, 1, 'TOYOTA YARIS', NULL, '', '10', '60', '180', NULL, 1, 'be8f4.jpg', 'Standard', '5', '2', '4', '1.5', 1, 1, 1, '', '', '', '', '', '', '', '', '', 1, 'toyota-yaris', '2016-10-24 05:42:29'),
(8, 0, 1, 'HYUNDAI ACCENT', NULL, '', '12', '70', '200', NULL, 1, 'd7737.jpg', 'Standard', '5', '2', '4', '1.6', 1, 1, 1, '', '', '', '', '', '', '', '', '', 1, 'hyundai-accent', '2016-10-24 05:51:46'),
(11, 0, 1, 'MITSUBISHI LANCER', NULL, '', '12', '75', '200', NULL, 1, '0c418.png', 'Standard', '5', '2', '4', '2.0', 1, 1, 1, '', '', '', '', '', '', '', '', '', 1, 'mitsubishi-lancer', '2016-10-26 05:43:37'),
(20, 1, 1, 'FORD FIGO (SEDAN)', NULL, NULL, '10', '10', '65', NULL, 1, '72cad.jpg', '180', '5', '2', '5', '1.5', 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ford-figo-sedan', '2016-12-04 01:06:50'),
(21, 1, 1, 'MAZDA 2', NULL, NULL, '10', '65', '180', NULL, 1, '2f660.png', '2016', '5', '2', '5', '1.5', 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'mazda-2', '2016-12-04 01:12:40'),
(22, 2, 1, 'GEELY', NULL, NULL, '9', '55', '160', NULL, 1, '99307.jpg', '2016', '5', '2', '4', '1.8', 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'geely', '2016-12-12 07:36:09'),
(23, 2, 1, 'MITSUBISHI LANCER FORTIS', NULL, NULL, '10', '65', '165', NULL, 1, 'f2d13.png', '2013', '5', '2', '5', '1.8', 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'mitsubishi-lancer-fortis', '2016-12-12 07:37:23'),
(24, 3, 1, 'TOYOTA COROLLA 2L', NULL, NULL, '15', '90', '240', NULL, 1, 'cda08.jpg', '2016', '5', '2', '4', '2.0', 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'toyota-corolla-2l', '2016-12-12 07:38:57'),
(25, 4, 1, 'HYUNDAI SONATA', NULL, NULL, '22', '130', '320', NULL, 1, '3c0ba.jpg', '2015', '5', '2', '4', '2.0', 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'hyundai-sonata', '2016-12-12 07:40:28'),
(26, 4, 1, 'NISSAN ALTIMA', NULL, NULL, '20', '120', '300', NULL, 1, '0634a.jpg', '2015', '5', '2', '4', '2.5', 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'nissan-altima', '2016-12-12 07:41:55'),
(27, 4, 1, 'HONDA ACCORD', NULL, NULL, '20', '120', NULL, NULL, 1, '39b27.jpg', '2015', '5', '2', '4', '2.0', 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'honda-accord', '2016-12-12 07:43:07'),
(28, 4, 1, 'TOYOTA CAMRY', NULL, NULL, '25', '150', '375', NULL, 1, '15e02.jpg', '2015', '5', '2', '4', '2.3', 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'toyota-camry', '2016-12-12 07:44:09'),
(29, 7, 1, 'HYUNDAI AZERA', NULL, NULL, '28', '150', '350', NULL, 1, 'eec90.png', '2015', '5', '2', '4', '3.3', 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'hyundai-azera', '2016-12-12 07:45:23'),
(30, 8, 1, 'GMC YUKON', NULL, NULL, '60', '360', '550', NULL, 1, '4ca74.jpg', '2015', '5', '2', '4', '5.3', 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'gmc-yukon', '2016-12-12 07:51:45'),
(31, 8, 1, 'LEXUS ES 350', NULL, '', '60', '390', '1000', NULL, 1, '28ed4.jpg', '2016', '5', '2', '4', '3.5', 1, 1, 1, '', '', '', '', '', '', '', '', '', 1, 'lexus-es-350', '2016-12-12 07:53:17'),
(32, 8, 1, 'BMW Z4 CONVERTIBLE', NULL, NULL, '70', '420', '1100', NULL, 1, '3dabe.png', '2014', '4', '2', '2', '3.0', 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'bmw-z4-convertible', '2016-12-12 07:54:27'),
(33, 8, 1, 'MERCEDES E 300', NULL, NULL, '100', '600', '1400', NULL, 1, '7995d.jpg', '2015', '5', '2', '4', '2.1', 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'mercedes-e-300', '2016-12-12 07:55:45'),
(34, 8, 1, 'MERCEDES C 200', NULL, NULL, '80', '500', '1200', NULL, 1, 'e92ef.jpg', '2015', '5', '2', '4', '2.0', 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'mercedes-c-200', '2016-12-12 07:56:43'),
(35, 6, 1, 'RANGE ROVER SPORT 2013', NULL, '', '80', '480', '1200', NULL, 1, 'b6043.jpg', '2013', '5', '2', '4', '5.0', 1, 1, 1, '', '', '', '', '', '', '', '', '', 1, 'range-rover-sport', '2016-12-12 07:58:03'),
(36, 6, 1, 'RANGE ROVER SPORT 2016', NULL, NULL, '220', '1300', NULL, NULL, 1, '8f920.jpg', '2016', '5', '2', '4', '3.0', 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'range-rover-sport-2016', '2016-12-12 07:59:18'),
(37, 6, 1, 'RANGE ROVER VOGUE', NULL, NULL, '250', '1500', NULL, NULL, 1, '34ab1.jpg', '2016', '5', '2', '4', '5.0', 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'range-rover-vogue', '2016-12-12 08:00:31'),
(38, 5, 1, 'BMW 740', NULL, NULL, '120', '720', '1600', NULL, 1, 'b41d0.jpg', '2015', '5', '2', '4', '3.0', 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'bmw-740', '2016-12-12 08:01:41'),
(39, 7, 1, 'VOLVO XC60', NULL, NULL, '45', '250', '480', NULL, 1, '76465.png', '2015', '5', '2', '4', '2.0', 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'volvo-xc60', '2016-12-14 01:38:33'),
(40, 4, 1, 'MITSUBISHI OUTLANDER 4*2', NULL, NULL, '30', '180', '320', NULL, 1, '7fdeb.jpg', '2014', '5', '2', '4', '2.4', 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'mitsubishi-outlander-4-2', '2016-12-14 01:41:34'),
(41, 2, 1, 'NISSAN SENTRA', NULL, NULL, '12', '72', '190', NULL, 1, '63f66.jpg', NULL, '5', '2', '4', '1.8', 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'nissan-sentra', '2016-12-14 01:55:41'),
(42, 1, 1, 'CITROEN', NULL, NULL, '9', '60', '160', NULL, 1, '60708.jpg', '2016', '5', '2', '4', '1.8', 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'citroen', '2016-12-14 02:03:21'),
(43, 1, 1, 'KIA CERATO', NULL, NULL, '10', '65', '180', NULL, 1, 'f5a7f.png', '2015', '5', '2', '4', '1.8', 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'kia-cerato', '2016-12-14 02:11:51');

-- --------------------------------------------------------

--
-- Table structure for table `rs_tbl_product_gallery`
--

CREATE TABLE IF NOT EXISTS `rs_tbl_product_gallery` (
  `gallery_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `file_name` varchar(150) DEFAULT NULL,
  `is_active` int(2) NOT NULL DEFAULT '1' COMMENT '1=>Active, 2=>InActive',
  `gallery_img_status` int(2) NOT NULL DEFAULT '2' COMMENT '1=>Main, 2=>other',
  PRIMARY KEY (`gallery_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `rs_tbl_questions`
--

CREATE TABLE IF NOT EXISTS `rs_tbl_questions` (
  `question_id` int(11) NOT NULL,
  `q_name` varchar(255) DEFAULT NULL,
  `q_email` varchar(255) DEFAULT NULL,
  `q_business_name` varchar(255) DEFAULT NULL,
  `q_phone` varchar(100) DEFAULT NULL,
  `q_website` varchar(100) DEFAULT NULL,
  `q_about_company` text,
  `q_identify_goal` int(5) DEFAULT NULL,
  `q_purpose_of_site` int(5) DEFAULT NULL,
  `q_time_frame` varchar(250) DEFAULT NULL,
  `q_willing__spend` varchar(250) DEFAULT NULL,
  `q_visitor_yur_site` int(5) DEFAULT NULL,
  `q_color_option` varchar(255) DEFAULT NULL,
  `q_logo_option` varchar(255) DEFAULT NULL,
  `q_add_features` text,
  `q_links_cop_1` varchar(255) DEFAULT NULL,
  `q_links_cop_2` varchar(255) DEFAULT NULL,
  `q_links_cop_3` varchar(255) DEFAULT NULL,
  `q_links_comm_1` varchar(255) DEFAULT NULL,
  `q_links_comm_2` varchar(255) DEFAULT NULL,
  `q_links_comm_3` varchar(255) DEFAULT NULL,
  `posted_date` datetime DEFAULT NULL,
  `read_status` int(2) NOT NULL DEFAULT '1' COMMENT '1=>UnRead, 2=>Read',
  PRIMARY KEY (`question_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
