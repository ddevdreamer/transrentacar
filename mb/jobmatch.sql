/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50527
Source Host           : localhost:3306
Source Database       : jobmatch

Target Server Type    : MYSQL
Target Server Version : 50527
File Encoding         : 65001

Date: 2013-10-30 17:19:57
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `rs_tbl_customer`
-- ----------------------------
DROP TABLE IF EXISTS `rs_tbl_customer`;
CREATE TABLE `rs_tbl_customer` (
  `customer_id` int(10) NOT NULL,
  `email` varchar(255) NOT NULL,
  `pass` varchar(32) NOT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `gender` int(2) DEFAULT '1' COMMENT '1=>Male, 2=>Female',
  `dob` date DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `zip_code` varchar(50) DEFAULT NULL,
  `country` int(10) DEFAULT NULL,
  `phone` varchar(30) DEFAULT NULL,
  `mobile` varchar(30) DEFAULT NULL,
  `reg_date` datetime DEFAULT NULL,
  `is_active` smallint(4) NOT NULL DEFAULT '0' COMMENT '1=>Active, 2=>InActive, 3=>Payment Pending, 4=>Expire',
  `customer_type` int(2) DEFAULT '1' COMMENT '1=>Student, 2=>Business, 3=>University, 4=>Recruiters',
  `profile_image` varchar(255) DEFAULT NULL,
  `sec_question` int(5) DEFAULT NULL,
  `sec_answer` text,
  `url_key` varchar(255) DEFAULT NULL,
  `package_id` int(11) DEFAULT NULL,
  `about_us` text,
  PRIMARY KEY (`customer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of rs_tbl_customer
-- ----------------------------
INSERT INTO `rs_tbl_customer` VALUES ('1', 'numan_tahir1@yahoo.com', 'a3f7a3d050ecaf39210582a7c0e747e0', 'Numan', 'Tahir', '1', '1987-09-17', 'Testing address', 'City Test', null, '', '0', null, '', '2013-09-30 17:33:17', '1', '1', null, null, null, 'numan-tahir1', null, 'sdfasd fasfd adf');
INSERT INTO `rs_tbl_customer` VALUES ('2', '1numan@elanist.com', 'a3f7a3d050ecaf39210582a7c0e747e0', 'Numan', 'Tahir', '1', '1984-07-17', 'address', 'asdasd', null, '234', '162', null, '12345678901', '2013-10-04 16:28:45', '1', '2', null, null, null, 'numan-1', null, null);
INSERT INTO `rs_tbl_customer` VALUES ('3', 'numan@elanist.com', 'a3f7a3d050ecaf39210582a7c0e747e0', 'Bilal', 'Zaheer', '1', '1981-11-16', 'sdf', 'sfsdf', null, '', '0', null, '', '2013-10-04 16:33:09', '1', '2', null, null, null, 'numan-2', null, null);
INSERT INTO `rs_tbl_customer` VALUES ('4', 'ali@yahoo.com', 'a3f7a3d050ecaf39210582a7c0e747e0', 'Ali', 'Nawaz', '1', '1985-10-15', 'TEsting Address', 'City', null, '', '0', null, '', '2013-10-07 13:15:18', '1', '1', 'ee925-4.', null, null, 'ali', null, '');

-- ----------------------------
-- Table structure for `rs_tbl_customer_mailbox`
-- ----------------------------
DROP TABLE IF EXISTS `rs_tbl_customer_mailbox`;
CREATE TABLE `rs_tbl_customer_mailbox` (
  `mail_id` int(11) NOT NULL AUTO_INCREMENT,
  `to_id` int(11) DEFAULT NULL COMMENT 'Email Sender',
  `from_id` int(11) DEFAULT NULL COMMENT 'Email Receiver',
  `job_id` int(11) DEFAULT NULL,
  `msg_title` varchar(255) DEFAULT NULL,
  `msg_detail` text,
  `msg_status` int(2) NOT NULL DEFAULT '1' COMMENT '1=>UnRead, 2=>Read',
  `msg_date` datetime DEFAULT NULL,
  `msg_to_delete` int(2) NOT NULL DEFAULT '1' COMMENT '1=>Show in OutBox, 2=>Hide in OutBox',
  `msg_frm_delete` int(2) NOT NULL DEFAULT '1' COMMENT '1=>Show in InBox, 2=>Hide in InBox',
  `tracking_code` varchar(15) DEFAULT NULL,
  `admin_message` int(2) NOT NULL DEFAULT '2' COMMENT '1=>Yes, 2=>No',
  PRIMARY KEY (`mail_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of rs_tbl_customer_mailbox
-- ----------------------------
INSERT INTO `rs_tbl_customer_mailbox` VALUES ('1', '1', '3', null, 'This is my second message', '<p>Long shadow effects became very popular with the flat design trend and a new solution pop-ups each day to simplify creating them.</p>\r\n\r\n<p>A free Photoshop extension, Long Shadows Generator, is probably the best solution for Photoshop users.</p>\r\n\r\n<p>It is pretty straightforward, just apply the shadow length, opacity, angle and set the opacity. It is all ready.</p>', '2', '2013-10-15 14:13:19', '1', '1', null, '2');
INSERT INTO `rs_tbl_customer_mailbox` VALUES ('2', '1', '1', null, 'This is my first message', '<p>Long shadow effects became very popular with the flat design trend and a new solution pop-ups each day to simplify creating them.</p>\r\n\r\n<p>A free Photoshop extension, Long Shadows Generator, is probably the best solution for Photoshop users.</p>\r\n\r\n<p>It is pretty straightforward, just apply the shadow length, opacity, angle and set the opacity. It is all ready.</p>', '2', '2013-10-14 19:19:01', '1', '1', null, '2');
INSERT INTO `rs_tbl_customer_mailbox` VALUES ('3', '1', '1', null, 'test message', 'test message', '2', '2013-10-15 00:00:00', '2', '1', null, '2');
INSERT INTO `rs_tbl_customer_mailbox` VALUES ('4', '3', '1', null, 'test message', 'test message', '2', '2013-10-15 00:00:00', '1', '1', null, '2');
INSERT INTO `rs_tbl_customer_mailbox` VALUES ('5', '1', '1', null, 'files', '', '2', '2013-10-30 00:00:00', '1', '2', '1383128974', '2');
INSERT INTO `rs_tbl_customer_mailbox` VALUES ('6', '1', '1', null, 'new files', '', '2', '2013-10-30 00:00:00', '1', '2', '1383129481', '2');
INSERT INTO `rs_tbl_customer_mailbox` VALUES ('7', '1', '1', null, 'new files', '<p>ccncoomdc</p><p>nckdnckndck</p><p>ckdcmd</p>', '2', '2013-10-30 00:00:00', '1', '1', '1383133972', '2');
INSERT INTO `rs_tbl_customer_mailbox` VALUES ('8', '1', '1', null, 'Re: new files', 'right back at you', '2', '2013-10-30 00:00:00', '1', '1', '1383135277', '2');

-- ----------------------------
-- Table structure for `rs_tbl_customer_mailbox_file`
-- ----------------------------
DROP TABLE IF EXISTS `rs_tbl_customer_mailbox_file`;
CREATE TABLE `rs_tbl_customer_mailbox_file` (
  `mail_file_id` int(11) NOT NULL AUTO_INCREMENT,
  `mail_id` int(11) DEFAULT NULL,
  `tracking_code` varchar(20) DEFAULT NULL,
  `file_name` varchar(200) DEFAULT NULL,
  `org_file_name` varchar(200) DEFAULT NULL,
  `file_size` varchar(30) DEFAULT NULL,
  `file_type` varchar(10) DEFAULT NULL,
  `is_active` int(2) DEFAULT '1' COMMENT '1=>Active, 2=>InActive',
  PRIMARY KEY (`mail_file_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of rs_tbl_customer_mailbox_file
-- ----------------------------
INSERT INTO `rs_tbl_customer_mailbox_file` VALUES ('1', null, '1383128974', '4199781aef.png', '2.4 Business Profile - new search - working hours.png', '314795', 'png', '1');
INSERT INTO `rs_tbl_customer_mailbox_file` VALUES ('2', null, '1383128974', '468bff1fae.pdf', 'Bug & suggestion Report.pdf', '426158', 'pdf', '1');
INSERT INTO `rs_tbl_customer_mailbox_file` VALUES ('3', null, '1383129481', '7b20c4e3.png', '2.4 Business Profile - new search - working hours.png', '314795', 'png', '1');
INSERT INTO `rs_tbl_customer_mailbox_file` VALUES ('4', null, '1383129481', 'de70b77c8f.pdf', 'Bug & suggestion Report.pdf', '426158', 'pdf', '1');
INSERT INTO `rs_tbl_customer_mailbox_file` VALUES ('5', null, '1383129978', '15f3901e60.png', '2.4 Business Profile - new search - working hours.png', '314795', 'png', '1');
INSERT INTO `rs_tbl_customer_mailbox_file` VALUES ('6', null, '1383129978', '7c4dd3a357.pdf', 'Bug & suggestion Report.pdf', '426158', 'pdf', '1');
INSERT INTO `rs_tbl_customer_mailbox_file` VALUES ('7', null, '1383130158', 'b9b453c4d4.jpg', 'Chrysanthemum.jpg', '879394', 'jpg', '1');
INSERT INTO `rs_tbl_customer_mailbox_file` VALUES ('8', null, '1383130158', 'c087ac4527.jpg', 'Desert.jpg', '845941', 'jpg', '1');
INSERT INTO `rs_tbl_customer_mailbox_file` VALUES ('9', null, '1383133972', 'f8902572c6.png', '2.4 Business Profile - new search - working hours.png', '314795', 'png', '1');
INSERT INTO `rs_tbl_customer_mailbox_file` VALUES ('10', null, '1383133972', 'f0290f68c.pdf', 'Bug & suggestion Report.pdf', '426158', 'pdf', '1');
INSERT INTO `rs_tbl_customer_mailbox_file` VALUES ('11', null, '1383135277', 'e42f205bf.docx', 'f8app.docx', '15002', 'docx', '1');

-- ----------------------------
-- Table structure for `rs_tbl_customer_mail_contact_list`
-- ----------------------------
DROP TABLE IF EXISTS `rs_tbl_customer_mail_contact_list`;
CREATE TABLE `rs_tbl_customer_mail_contact_list` (
  `customer_msg_contact_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `contact_id` int(11) NOT NULL,
  `msg_contact_date` date DEFAULT '0000-00-00',
  `is_active` int(2) NOT NULL DEFAULT '1' COMMENT '1=>Active, 2=>InActive',
  PRIMARY KEY (`customer_msg_contact_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of rs_tbl_customer_mail_contact_list
-- ----------------------------
INSERT INTO `rs_tbl_customer_mail_contact_list` VALUES ('0', '1', '1', '0000-00-00', '1');
INSERT INTO `rs_tbl_customer_mail_contact_list` VALUES ('2', '1', '2', '0000-00-00', '1');
INSERT INTO `rs_tbl_customer_mail_contact_list` VALUES ('3', '1', '3', '0000-00-00', '1');
INSERT INTO `rs_tbl_customer_mail_contact_list` VALUES ('4', '1', '4', '0000-00-00', '1');
