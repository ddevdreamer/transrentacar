<?php $customer_id=1; ?>
<!DOCTYPE html>
<html>
<head>
    <title>Job MAtch</title>
    <link rel="stylesheet" type="text/css" href="includes/css/style.css" />
     <link rel="stylesheet" href="style.css" type="text/css" media="screen" charset="utf-8" />
     <!-- Bootstrap styles -->


<!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->

        <script src="includes/js/prototype.js" type="text/javascript"></script>
    <script src="includes/js/stars.js" type="text/javascript"></script>
    <script type="text/javascript" src="http://js.nicedit.com/nicEdit-latest.js"></script> <script type="text/javascript">
//<![CDATA[
        bkLib.onDomLoaded(function() { nicEditors.allTextAreas() });
  //]]>
  </script>
   <script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.6.min.js" type="text/javascript" charset="utf-8">
        </script>

        <script src="jquery.fcbkcomplete.js" type="text/javascript" charset="utf-8">
        </script>
        <script>

	function show_inbox(cust_id){
		$('#reply').hide('slow');
		$('#delete_link').hide('slow');
		$.ajax({
  type: "POST",
  url: "inbox_count.php",
  data: {customer_id: cust_id}
}).done(function( msg ) {
	$('#inbox_count').html(msg);
});
        		$.ajax({
  type: "POST",
  url: "inbox.php",
  data: {customer_id: cust_id}
}).done(function( msg ) {
	$('#emailInnerSection').html(msg);
});
	}
	
//////////////////////////////////////////////////////////////////////////	

	function sent(cust_id){
		$('#reply').hide('slow');
		$('#delete_link').hide('slow');
		
        		$.ajax({
  type: "POST",
  url: "sent.php",
  data: {customer_id: cust_id}
}).done(function( msg ) {
	$('#emailInnerSection').html(msg);
});
	}
	
//////////////////////////////////////////////////////////////////////////	

	function search_inbox(cust_id){
		$('#reply').hide('slow');
		$('#delete_link').hide('slow');
		var search_term=$('#search').val();
		
        		$.ajax({
  type: "POST",
  url: "search.php",
  data: {customer_id: cust_id, st: search_term}
}).done(function( msg ) {
	$('#emailInnerSection').html(msg);
});
	}
	
	
//////////////////////////////////////////////////////////////////////////	

	function deleted(cust_id){
		$('#reply').hide('slow');
		$('#delete_link').hide('slow');
		
        		$.ajax({
  type: "POST",
  url: "deleted.php",
  data: {customer_id: cust_id}
}).done(function( msg ) {
	$('#emailInnerSection').html(msg);
});
	}

	
//////////////////////////////////////////////////////////////////////////	

function delete_msg(){
		var viewed=$('#viewed').val();
		var s=$('#soi').val();
        		$.ajax({
  type: "POST",
  url: "delete.php",
  data: {mail_id: viewed, soi: s}
}).done(function( msg ) {
	alert('Mail Deleted');
	show_inbox(<?php echo $customer_id; ?>);
});
	}
	
//////////////////////////////////////////////////////////////////////////	

	function show_reply(){
		$('#reply_msg').show('slow');		
	}
	
//////////////////////////////////////////////////////////////////////////	

	function view_message(m_id){
		var s=$('#soi').val();
		if(s==1){
		$('#reply').show('slow');
		}
		$('#delete_link').show('slow');
		
		$.ajax({
  type: "POST",
  url: "view_message.php",
  data: {mail_id : m_id, c: <?php echo $customer_id; ?>, soi: s}
}).done(function( msg ) {
	$('#emailInnerSection').html(msg);

});
     
	}
////////////////////////////////////////////////////////////////////////////	
	function new_message(){
		
		$.ajax({
  type: "POST",
  url: "new_message.php"
}).done(function( msg ) {
	$('#emailInnerSection').html(msg);
	 $("#select3").fcbkcomplete({
                    json_url: "data.php/?c=<?php echo $customer_id; ?>",
                    addontab: true,                   
                    input_min_size: 0,
                    height: 10,
                    cache: true,
                    newel: false,
                    select_all_text: "select",
					onselect: getSelected,
                });
				function getSelected() {
           var lastValue = $('#select3 option:last-child').val();
		   var num=$('.bit-box').length;
		   var cou=0;
		   $('.bit-box').each(function(index, element) {
			   cou++;
			 if(cou < num){  
            	if($(this).attr('rel')==lastValue){
					$('.bit-box:last').remove();
				}
			 }
        });
            
		   
        }
		
});
     
	}
//////////////////////////////////////////////////////////////	
	function send_msg(){
		
		var ids='';
		var num=$('#select3 option').length;
		   var cou=0;
		$('#select3 option').each(function(index, element) {
			   cou++;
			   
			 if(cou < num){  
            	ids=ids+$(this).attr('value')+',';
			 }
			  if(cou == num){  
            	ids=ids+$(this).attr('value');
			 }
        });
	var subject=$('#subject_msg').val();
	var nicE = new nicEditors.findEditor('message'); 
	var message=nicE.getContent();
	 var tracking_code=$('#tracking_code').val();
	$.ajax({
  type: "POST",
  url: "send_message.php",
  data: {to_ids : ids, subject: subject, message: message, from_id: <?php echo $customer_id; ?>, tr: tracking_code}
}).done(function( msg ) {
	alert('Mail Sent');
	show_inbox(<?php echo $customer_id; ?>);
});
	}
	</script>
</head>
<body>
    <header id="mainHeader">
        <div class="wrapperDiv">
            <div class="logoDiv">
                <a href="index.html">
                    <img src="includes/imgs/jobmatch_logo.jpg" alt="Logo" />
                </a>
            </div>
            <ul id="topNav">
                <li><a href="#.">PROFILE</a></li>
                <li><a href="#.">GUIDE</a></li>
                <li><a href="#.">FAQ's</a></li>
                <li><a href="#.">HELP!</a></li>
            </ul>
            <div class="topRight">
                <div class="searchDiv">
                    <input type="text" name="name" class="txtTop">
                    <button type="button" class="btnTop roundcornerz">
                        Search</button>
                </div>
                <ul class="topRightLink">
                    <li><a href="#.">
                        <img src="includes/imgs/msg-icon.jpg" />
                    </a></li>
                    <li><a href="#.">Sign Out</a></li>
                </ul>
            </div>
            <div class="clr">
            </div>
        </div>
    </header>
    <div class="clr">
    </div>
    <div class="wrapperDiv">
        <section id="mainSection">
            <aside class="topAside">
                <ul class="photoLinks">
                    <li><a href="#.">Phil Casabon</a></li>
                    <li><a href="#.">Edit</a></li>
                </ul>
                <div class="clr">
                </div>
                <div class="imageFrame">
                </div>
            </aside>
            <section class="topRightSection">
                <section class="topThreeSections">
                    <span>Profile Completion</span>
                    <div class="completionDiv">
                        <div class="percents">
                            55%</div>
                    </div>
                    <span class="profileSpan">Complete Profile</span>
                </section>
                <section class="topThreeSections">
                    <span class="secondSpan">WHO HAS VIEW YOU</span>
                </section>
                <section class="topThreeSections starSection">
                    <span>RATING</span>
                    <div class="ratingStars">
                        <script type="text/javascript">
                            var s1 = new Stars({
                                maxRating: 5,
                                imagePath: 'includes/imgs/stars/',
                                value: 5
                            });
									</script>	
                    </div>
                    <span class="ratingSpan">View ratings and testimonials </span>
                </section>
                <div class="clr">
                </div>
                <nav class="mainNavigation">
                    <ul>
                        <li><a href="#.">PROFILE</a></li>
                        <li><a href="#.">JOBS</a></li>
                        <li class="active"><a href="#.">MESSAGES</a></li>
                        <li><a href="#.">SETTINGS</a></li>
                    </ul>
                </nav>
            </section>
            <div class="clr">
            </div>
            <aside class="midLeftAside">
                <section class="informationSection">
                    <div class="headingBg">
                        <h2>
                            INFORMATION</h2>
                    </div>
                    <span class="informationSpan">Registered:10/3/2013</span> <span class="informationSpan">
                        Registered:10/3/2013</span> <span class="informationSpan">Registered:10/3/2013</span>
                    <span class="informationSpan">Registered:10/3/2013</span>
                </section>
                <section class="informationSection informationSection2">
                    <div class="headingBg">
                        <h2>
                            INFORMATION</h2>
                    </div>
                    <a href="#." class="facebookLinks">facebook.com/MyJobMatch</a><a href="#." class="facebookLinks twitterLinks">twitter.com/MyJobMatch</a><a
                        href="#." class="facebookLinks socialLinks"></a>
                </section>
            </aside>
            <style type="text/css">
			#inbox_list{float:left; width:100%;}
			#inbox_list li{float:left; width:100%; padding:10px 0; border-bottom: solid 1px lightgrey;}
			#inbox_list li span{float:left; font-family: "Segoe UI Web Semibold"; font-size: 104%;}
			.msg_from{ color:black;font-weight: bold; margin:0 15px; width:24%}
			.msg_subject{color: #0072C6; width:53%;}
			.msg_date{ font-weight:normal; color:black;  width:15%;}
			#emailInnerSection table{ float:left; color:grey; margin:25px 0 0 25px;}
			#emailInnerSection table td{ padding:10px 0;}
			#emailInnerSection table td input{ width:200px;}
			#emailInnerSection table td textarea{ width:300px;}
			#view_message{ float:left; width:90%; color: #444444; font-family: "Segoe UI Web Semibold"; padding:20px;}
			#view_message h1{ float:left;border-bottom: solid 1px lightgrey; width:100%; font-size:150%; padding-bottom:10px;}
			.from_msg{ float:left;margin-top:10px; font-size:110%; width:100%;}
			.message{float:left; width:100%; margin-top:15px; font-weight:normal; font-size:110%;}
			.send_btn{float: left;margin-left: 129px;color: white;background-color: #0072C6;padding: 5px 10px;}
			</style>
            
            <section class="midRightSection">
                <section class="emailSection">
                    <div class="emailTop">
                        <a href="#." class="plusEmail" onClick="new_message()">+</a> <a href="#." class="new"  onClick="new_message()">New</a> <a href="#."
                            class="reply" id="reply" style="display:none" onclick="show_reply()">Reply</a><!--<a href="#." class="vLink">V</a> --><a href="#." class="reply delete" style="display:none" id="delete_link" onClick="delete_msg()">Delete</a> <a href="#." onClick="show_inbox(<?php echo $customer_id; ?>)" class="refreshImg" ><img src="includes/imgs/refresh.jpg" alt="" /></a>
                    </div>
                    <aside class="emailAside">
                        <div class="txtEmailBg">
                        
                            <input type="text" name="name" id="search" class="txtTop txtEmail">
                            <a href="#." onClick="search_inbox(<?php echo $customer_id; ?>)" style="position: absolute;margin-left: -33px;margin-top: 3px;" ><img src="includes/imgs/search-icon.jpg"></a>
                            <div class="clr">
                            </div>
                                             <?php
											 
											 
                    $con = mysql_connect("localhost","root","");
if (!$con)
  {
  die('Could not connect: ' . mysql_error());
  }

mysql_select_db("jobmatch", $con);
$result=mysql_query("SELECT *  from rs_tbl_customer_mailbox where msg_frm_delete='1' and to_id='".$customer_id."'");
?>
                            <a class="inboxSpan" href="#." onClick="show_inbox(<?php echo $customer_id; ?>)">Inbox ( <span id="inbox_count"><?php echo mysql_num_rows($result); ?></span> )</a> <a href="#." class="inboxSpan inboxSpan2" onClick="sent(<?php echo $customer_id; ?>)">Sent</a> <a href="#." onClick="deleted(<?php echo $customer_id; ?>)" class="inboxSpan inboxSpan2">
                                Deleted</a>
                        </div>
                    </aside>
                    <section id="emailInnerSection">
                    <ul id="inbox_list">
  <?php

while($row=mysql_fetch_array($result)){
$result1=mysql_query("SELECT first_name,last_name  from rs_tbl_customer where customer_id='".$row['from_id']."'");
while($row1=mysql_fetch_array($result1)){
	$name=$row1['first_name'].' '.$row1['last_name']; 
}
$fr_style='';
$sub_style='';
if($row['msg_status']==2){
	$fr_style='style=" font-weight:normal;"';
	$sub_style='style=" color:black; font-weight:normal;"';
}

    echo            '<li><a href="#." onClick="view_message('.$row['mail_id'].')">
                    <span class="msg_from" '.$fr_style.'>'.$name.'</span>
                    <span class="msg_subject" '.$sub_style.'>'.$row['msg_title'].'</span>
                    <span class="msg_date">'.date('m/d/Y',strtotime($row['msg_date'])).'</span></a>
                    </li>';
}
?>
                    </ul>;
                    <input type="hidden" id="soi" value="1">
                        <a href="#." class="scrollRight">
                            <img src="includes/imgs/scroller.jpg" />
                        </a>
                        <div class="clr">
                        </div>
                    </section>
                    <input type="hidden" name="customer_id" id="customer_id" value="<?php echo $customer_id; ?>">
                    <div class="clr">
                    </div>
                </section>
                <section class="recentNewsSection">
                    <a href="#." class="recentNews">
                        <img src="includes/imgs/recent-news.jpg" />
                    </a>
                    <section class="recentNewsUpdate">
                Keep up to update with all the recent news at Job Match
                </section>
                </section>
            </section>
            <div class="clr">
            </div>
        </section>
    </div>
    <div class="clr">
    </div>
    <footer class="footerBg">
        <div class="wrapperDiv">
            <ul class="footerLinks">
                <li><a href="#.">TERMS & CONDITION</a></li>
                <li><a href="#.">RETURN POLICY</a></li>
                <li><a href="#.">PRIVACY POLICY</a></li>
                <li><a href="#.">FAQ</a></li>
            </ul>
            <div class="footerRight">
                &copy; 2012 COPYRIGHT JOBMATCH WEBSITE BY <a href="#.">DOUBLE DUECE</a>
            </div>
        </div>
    </footer>
</body>
</html>
