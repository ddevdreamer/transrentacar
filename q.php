<?php
require_once("config/config.php");
$objCommon 		= new Common;
$objMail 		= new Mail;
$objContent 	= new Content;
$objValidate 	= new Validate;
$objTemplate 	= new Template;
$objAdminUser 	= new AdminUser;
if($_POST["q_name"]!=''){
$q_name 			= trim($_POST["q_name"]);
$q_email 			= trim($_POST["q_email"]);
$q_business_name 	= trim($_POST["q_business_name"]);
$q_phone 			= trim($_POST["q_phone"]);
$q_website 			= trim($_POST["q_website"]);
$q_about_company 	= trim($_POST["q_about_company"]);
$q_identify_goal 	= trim($_POST["q_identify_goal"]);
$q_purpose_of_site 	= trim($_POST["q_purpose_of_site"]);
$q_time_frame 		= trim($_POST["q_time_frame"]);
$q_willing__spend 	= trim($_POST["q_willing__spend"]);
$q_visitor_yur_site = trim($_POST["q_visitor_yur_site"]);
$q_color_option 	= trim($_POST["q_color_option"]);
$q_logo_option 		= trim($_POST["q_logo_option"]);
$q_add_features 	= trim($_POST["q_add_features"]);
$q_links_cop_1 		= trim($_POST["q_links_cop_1"]);
$q_links_cop_2 		= trim($_POST["q_links_cop_2"]);
$q_links_cop_3 		= trim($_POST["q_links_cop_3"]);
$q_links_comm_1 	= trim($_POST["q_links_comm_1"]);
$q_links_comm_2 	= trim($_POST["q_links_comm_2"]);
$q_links_comm_3 	= trim($_POST["q_links_comm_3"]);

	$question_id = $objAdminUser->genCode("rs_tbl_questions", "question_id");
	$objContent->setProperty("question_id", $question_id);
	$objContent->setProperty("q_name", $q_name);
	$objContent->setProperty("q_email", $q_email);
	$objContent->setProperty("q_business_name", $q_business_name);
	$objContent->setProperty("q_phone", $q_phone);
	$objContent->setProperty("q_website", $q_website);
	$objContent->setProperty("q_about_company", $q_about_company);
	$objContent->setProperty("q_identify_goal", $q_identify_goal);
	$objContent->setProperty("q_purpose_of_site", $q_purpose_of_site);
	$objContent->setProperty("q_time_frame", $q_time_frame);
	$objContent->setProperty("q_willing__spend", $q_willing__spend);
	$objContent->setProperty("q_visitor_yur_site", $q_visitor_yur_site);
	$objContent->setProperty("q_color_option", $q_color_option);
	$objContent->setProperty("q_logo_option", $q_logo_option);
	$objContent->setProperty("q_add_features", $q_add_features);
	$objContent->setProperty("q_links_cop_1", $q_links_cop_1);
	$objContent->setProperty("q_links_cop_2", $q_links_cop_2);
	$objContent->setProperty("q_links_cop_3", $q_links_cop_3);
	$objContent->setProperty("q_links_comm_1", $q_links_comm_1);
	$objContent->setProperty("q_links_comm_2", $q_links_comm_2);
	$objContent->setProperty("q_links_comm_3", $q_links_comm_3);
	$objContent->setProperty("posted_date", date('Y-m-d H:i:s'));
	$objContent->setProperty("read_status", 1);
	if($objContent->actQuestions('I')){
	$arr = array('pass' => 'success');
	echo json_encode($arr);
	}
}
?>