<?php
require_once("config/config.php");
$objCommon 		= new Common;
$objMail 		= new Mail;
$objContent 	= new Content;
$objValidate 	= new Validate;
$objTemplate 	= new Template;

if($_SERVER['REQUEST_METHOD'] == "POST"){
	$cot_email			= $_POST['cot_email'];
	$cot_name			= $_POST['cot_name'];
	$cot_subject		= $_POST['cot_subject'];
	$cot_message		= $_POST['cot_message'];
	$adminemail 		= ADMIN_EMAIL;
	
			// Send mail to admin
			$content 		= '<table width="100%" border="0" cellpadding="3" cellspacing="1">
								  <tr>
									<td colspan="3">Following message has been posted from the contact page of Source-Station.</td>
								  </tr>
								  <tr>
									<td width="19%">&nbsp;</td>
									<td width="1%">&nbsp;</td>
									<td width="80%">&nbsp;</td>
								  </tr>
								  <tr>
									<td align="left" valign="top">' . _E_MAIL . '</td>
									<td align="left" valign="top">:</td>
									<td align="left" valign="top">' . $cot_email . '</td>
								  </tr>
								  <tr>
									<td align="left" valign="top">' . _FULLNAME . '</td>
									<td align="left" valign="top">:</td>
									<td align="left" valign="top">' . $cot_name . '</td>
								  </tr>
								  <tr>
									<td align="left" valign="top">' . _MESSAGE . '</td>
									<td align="left" valign="top">&nbsp;</td>
									<td align="left" valign="top">' . $cot_message . '</td>
								  </tr>
								</table>';
	
			$body 			= file_get_contents(TEMPLATE_URL . "template.php");
			$body			= str_replace("[BODY]", $content, $body);
				
			$objMail		= new Mail;
			$objMail->IsHTML(true);
			$objMail->setSender($cot_email, $cot_name);
			$objMail ->AddEmbeddedImage(TEMPLATE_PATH . "banner_email.jpg", 1, 'banner_email.jpg');
			$objMail->setSubject($cot_subject);
			$objMail->AddEmbeddedImage("banner_email.jpg", 1);
			$objMail->setReciever($objCommon->getConfigValue("site_email"), SITE_NAME);
			$objMail->setBody($body);
			$objMail->Send();
			
			$arr = array('pass' => 'success');
			echo json_encode($arr);
		}
