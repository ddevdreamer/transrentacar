<?php
// title
define('_META_TITLE', "agro trading");

// header
define('_CHOOSE_LANGUAGE', 'Kies een taal:');
define('_REGISTER_NOW', 'Registreren');
define('_LOGIN', 'Inloggen');
define('_HOME', 'Home');
define('_WELCOME', 'Welkom');
define('_LOGOFF', 'Uitloggen');
define('_MY_ACCOUNT', 'Mijn Account');
define('_BTN_ZOEKEN', 'Zoeken');
define('_CHANGE_PASSWORD', 'Watchwoord Wijzigen');

// menu
define('_ALL_PRODUCTS', 'Alle producten');
define('_TopTen_PRODUCTS', 'Top Tien Products');
define('_OVER_ONS', 'Over ons');
//define('_WEBSHOP', 'Webshop');
//define('_SHOWROOM', 'Showroom');
//define('_TARIEVEN', 'Tarieven');
define('_SUBSCRIBE', 'Aanmelden');
define('_TOPTEN', 'Top Tien');
define('_PARTNERS', 'Partners');
define('_SITEMAP', 'Site map');
define('_CONTACTUS', 'Contact');
define('_CLIENT', 'Klant');


define('_INSTALLATIE', 'Installatie');
define('_SERVICE', 'Service');
define('_ZOEKEN', 'Zoeken');
define('_CONTACT', 'Contact');
define('_OFFERTE', 'Offerte');

define('_VIEW_SHOPPING_BAG', 'Winkelmandje');
define('_NUM_OF_ITEMS', 'items');
define('_MSG_WISHLIST_EMPTY', 'Uw wishlist is leeg.');
// left
define('_PRODUCTS', 'Producten');

// footer
define('_COPYRIGHTS', 'Copyright &copy; 2010 <a href="http://www.agrotrading.eu">Agro Trading B.V.</a>. All right reserved.');
define('_TERMS', 'Algemene voorwaarden');
define('_PRIVACY', 'Privacy Policy');
define('_POWERED_BY', 'Powered by: <a target="_blank" href="http://www.webciters.com/">WEBCITERS</a>');
define('_t', 't: +31 (0) 251 217250');

// home page
define('_HOME_PRODUCT', 'Home Products');
define('_VIEW_DETAILS', 'Details');
define('_ADD_TO_CART', 'Naar de winkelmand');
define('_ADD_TO_WISHLIST', 'Naar de wishlist');
define('_PRIVACY', 'Privacybeleid');
define('_HOME_SLOGAN', "agro trading");

// 404 page
define('_ACCESS_DENIED', 'Toegang geweigerd!');
define('_PAGENOT_FOUND_MSG', 'U heeft geen toegang tot het gebruik van deze pagina of pagina niet gevonden. Kunt u contact opnemen met de beheerder.');

// collection
define('_PRODUCTS', 'Producten');
define('_PRODUCT_LIST', 'Productlijst');
define('_NO_PRODUCTS_FOUND', 'No product(s) found.');
define('_Discount_Text', ' en gratis verzending');


// product details page
define('_PRODUCT_DETAILS', 'Details');
define('_SELECT_SIZE', 'Selecteren');
define('_MSG_SELECT_PRODUCT_SIZE', 'Selecteer een maat');
define('_MSG_PRODUCT_SOLD_OUT', 'Uitverkocht');
define('_SOLD_OUT', 'Uitverkocht');
define("_PRINT", "Afdrukken");
define("_SEND_TO_FRINTD", "Mail dit product");

// newsletter
define('_SEX', 'Geslacht');
define('_SEX_MALE', 'Man');
define('_SEX_FEMALE', 'Vrouw');
define('_INITIALS', 'Voorletters');
define('_NAME', 'Naam');
define('_EMAIL', 'E-mail');
define('_HOMETOWN', 'Plaats');
define('_SUBSCRIBE', 'Aanmelden');
define('_UNSUBSCRIBE', 'Afmelden');
define('_VLD_EMAIL', 'E-mail is verplicht.');
define('_VLD_NAME', 'Naam is verplicht.');
define('_VLD_INITIALS', 'Voorletters is verplicht.');
define('_VLD_HOMETOWN', 'Plaats is verplicht.');
define('_MSG_ALREADY_SUBSCRIBED', 'U bent al aangemeld!');
define('_MSG_SUCCESSFULLY_SUBSCRIBED', 'Uw abonnement informatie is opgeslagen. Echter, je moet bevestigen uw abonnement. Controleer uw e-mail voor meer informatie.');
define('_MSG_UNSCRIBPTION_NEED_CONFIRM', 'U moet bevestigen dat uw afmeldingsverzoek. Controleer uw e-mail voor meer informatie.');
define('_EMAIL_NOT_FOUND', 'Emailadres niet gevonden.');
define('_SUBSCRIBED_AND_ACTIVATED', 'U hebt aangemeld en  uw account geactiveerd.');
define('_UNSUBSCRIBED_SUCCESSFUL', 'U hebt afgemeld en uw email is verwijdered van ons system.');

// media
define('_READ_MORE', 'Lees meer');

// news details
define('_NEWS_DETAILS', 'Nieuws Details');

// contact
define('_YOUR_INFO', 'Uw informatie');
define('_E_MAIL', 'E-mail');
define('_FULLNAME', 'Naam');
define('_ADDRESS', 'Adres');
define('_COUNTRY', 'Land');
define('_SUBJECT', 'Onderwerp');
define('_MESSAGE', 'Bericht');
define('_SECURITY_CODE', 'Beveiligingsode');
define('_BTN_SUBMIT', 'Verstuur');
define('_BTN_CANCEL', 'Annuleren');
define('_SELECT_COUNTRY', 'Selecteer een land');

define('_CONT_VLD_EMAIL', 'E-mail is ongeldig.');
define('_CONT_VLD_FULLNAME', 'Naam is verplicht.');
define('_CONT_VLD_COUNTRY', 'Land is verplicht.');
define('_CONT_VLD_SUBJECT', 'Onderwerp is verplicht.');
define('_CONT_VLD_MESSAGE', 'Bericht is verplicht.');
define('_CONT_VLD_SECURITY_CODE', 'verificatiecode is verplicht.');
define('_CONT_VLD_INVALID_SECURITY_CODE', 'Onjuiste verificatiecode!');
define('_CONT_TOP_MESSAGE', "Bericht van agro trading website.");
define('_CONT_SUBMITTED_SUCCESSFULLY', 'Hartelijk dank voor uw mail. Wij zullen zo spoedig mogelijk contact met u opnemen.');

// cart
define('_VIEW_CART', 'Winkelwagen');
define('_MSG_REMOVE_ITEM', 'Verwijderen?');
define('_MSG_QTY_POSITIVE', 'Aantal moet een positief getal zijn.');
define('_COL_DESCRIPTION', 'Omschrijving');
define('_COL_QTY', 'Aantal');
define('_COL_PRICE', 'Prijs');
define('_COL_TOTAL', 'Totaal');
define('_LNK_VIEW_DETAILS', 'Bekijken');
define('_LNK_REMOVE_ITEM', 'Verwijderen');
define('_COL_ITEM_TOTAL', 'Item Totaal');
define('_COL_TAX_19_PER', 'BTW @19%');
define('_COL_GRAND_TOTAL', 'Te betalen');
define('_EMPTY_CART', 'Uw winkelmand is leeg.');
define('_WE_ACCEPT', 'Accepteren');

define('_BTN_CONTINUE_SHOPPING', 'Verder winkelen');
define('_BTN_UPDATE_CART', 'Update winkelwagen');
define('_BTN_CHECKOUT', 'Naar de kassa');

// login
define('_LOGIN_INFO', 'Inloggen');
define('_LOGIN_EMAIL', 'Emailadres');
define('_LOGIN_PWD', 'Wachtwoord');
define('_LOGIN_BTN', 'Login');
define('_LOGIN_FORGOT', 'Wachtwoord vergeten?');
define('_CHANGE_PASSWORD', 'Wachtwoord wijzigen');

define('_LOGIN_NEW', "Nieuwe klant?");
define('_LOGIN_NEW_MSG', "Nieuwe klant? Na een paar eenvoudige stappen is uw registratie voltooid. Klik op de onderstaande knop om verder te gaan.");
define('_LOGIN_FORGOT', 'Wachtwoord vergeten?');
define('_LOGIN_FORGOT_MSG', 'Wachtwoord vergeten? Geen probleem.  Binnen een paar seconden ontvangt u deze per email.');
define('_LOGIN_LNK_REGISTER_NOW', 'Nu registreren');
define('_LOGIN_LNK_REQUEST_NOW', 'Aanvragen');

define('_VLD_INVALID_EMAIL', 'Emailadres is verplicht.');
define('_VLD_PASSWORD', 'Wachtwoord is verplicht.');
define('_LOGIN_INVALID_LOGIN', 'Ongeldige aanmeldgegevens. Probeer het opnieuw.');
define('_CUST_ACCOUNT_SUSPENDED', 'Uw account is opgeschort. Contact beheerder.');

// forgot
define('_FORGOT_PASWORD', 'Wachtwoord vergeten');
define('_FORGOT_EMAIL_NOT_VALID', 'Emailadres bestaat niet.');
define('_FORGOT_CUSSESS', 'Uw wachtwoord is gereset en verzonden naar uw e-mailadres. Controleer yor email. U kunt nu <a href="' . SITE_URL . 'login/" style="color:#000;">inloggen</a> met gebruikmaken van uw nieuwe wachtwoord.');

// register
define('_REGISTRATION', 'Registratie');
define('_REG_SUCCESS', 'Je bent geregistreerd. Je kan nu inloggen en doorgaan met winkelen.');
define('_REG_LOGIN_INFO', 'Login informatie');
define('_REG_EMAIL', 'Emailadres');
define('_REG_PASSWD', 'Wachtwoord');
define('_REG_CONFIRM_PASSWD', 'Bevestig wachtwoord');
define('_REG_PERSONAL_INFO', 'Persoonlijke gegevens');
define('_REG_TITLE', 'Aanhef dhr/ mevr.');
define('_REG_FIRST_NAME', 'Voornaam');
define('_REG_LAST_NAME', 'Achternaam');
define('_REG_BILLING_INFO', 'Factuur gegevens');
define('_REG_ADD_1', 'Straatnaam en huisnummer');
define('_REG_ADD_2', 'Adres');
define('_REG_CITY', 'Plaats');
define('_REG_COUNTY', 'Provincie');
define('_REG_ZIP', 'Postal/Zip');
define('_REG_COUNTRY', 'Land');
define('_REG_DAY_PHONE', 'Telefoon overdag');
define('_REG_MOBILE', 'Mobiel');
define('_REG_SAME_BILLING', 'Mijn factuur adres is anders dan het bezorgadres');
define('_REG_SHIPPING_INFO', 'Bezorginformatie');
define('_REG_SECURITY', 'Beveiliging');
define('_REG_SECURITY_CODE', 'Beveiligingscode');
define('_REG_REGISTER', 'Registreren');
define('_REG_INVALID_SECURITY_CD', 'Onjuiste verificatiecode!');
define('_REG_CONFIRM_PWD_NOT_MATCHED', 'Wachtwoord komt niet overeen.');
define('_REG_EMAIL_ALREADY_EXISTS', 'Emailadres al in gebruik.');

define('_IS_REQUIRED_FLD', ' is verplicht.');

// checkout
define('_CHECKOUT', 'Checkout');
define('_FREE', 'Free');
define('_CHECKOUT_CONTACT', 'Contact');
define('_BILLING_ADDRESS', 'Factuuradres');
define('_ORDER_DATE', 'Besteldatum');
define('_SHIPPING_ADDRESS', 'Bezorgadres');
define('_CHECKOUT_CHANGE', 'Wijzigen');
define('_JS_MSG_SELECT_SHIPPING_TYPE', 'Selecteer een bezorgmethode.');
//define('_JS_MSG_SELECT_PAYMENT_METHOD', 'Selecteer betalingsmethode: \n- iDEAL \n- PayPal \n- Vooruitbetaling \n- Rembourse');
define('_JS_MSG_SELECT_PAYMENT_METHOD', 'Please select payment method: \n- iDEAL \n- Advance Payment \n- Factuur');
define('_SELECT_SHIPPING_TYPE', 'Selecteer een bezorgmethode.');
define('_PAYMENT_IDEAL', 'iDEAL');
define('_PAYMENT_PAYPAL', 'Paypal');
define('_PAYMENT_ADV', 'Vooruit betalen');
define('_PAYMENT_CASH_ON_DELIVERY', 'Rembourse');

// ideal payment page
define('_IDEAL_CHECKOUT', 'iDEAL');
define('_IDEAL_SHIPPING', 'Verzending');
define('_IDEAL_DELIVERY_TIME', 'Verzendtijd');
define('_IDEAL_SHIPPING_CHARGE', 'Verzendkosten');
define('_FREE_BEFORE', 'gratis als meer dan');
define('_FREE_AFTER', 'purchase');

define('_TOTAL_PURCHASE_ITEMS', 'Totaal gekochte');
define('_IS_REQUIRED_FLD', ' is verplicht.');
define('_IS_I_AM', 'I am a.');
define('_REG_CUSTOMERTYPE_1', 'Wholesaler');
define('_REG_CUSTOMERTYPE_2', 'Retailer');
define('_REG_COMPANYNAME', 'Your Company Name ');
define('_REG_KVK', 'Ch. of Commerce reg');
define('_REG_TAX', 'VAT Number');
define('_REG_FAX', 'Fax');
// paypal checkout page
define('_PAYPAL_CHECKOUT', 'Paypal');

define("_JS_FORM_ERROR", "Een van volgende:");

// advanced payment page
define('_ADV_CHECKOUT', 'Vooruit betalen');
define('_ADV_BANK_DETAILS', 'Bank Details');
define('_ADV_BANK_NAME', 'Naam bank');
define('_ADV_ACC_NAME', 'Naam begunstigde');
define('_ADV_ACC_NUM', 'Rekeningnummer');
define('_ADV_AGREEMENT_TEXT', "Ik ga akkoord met vooruitbetaling aan agro trading.");

define('_JS_MSG_ADV_BANK_NAME', 'Bank Naam is verplicht.');
define('_JS_MSG_ADV_ACC_NAME', 'Account Naam is verplicht.');
define('_JS_MSG_ADV_ACC_NUM', 'Rekeningnummer is verplicht.');
define('_JS_MSG_ADV_AGREEMENT', "U moet instemmen met automatische incasso");

// cash on delivery page
define('_CASH_ON_DELIVERY', 'Rembours');

// payment response/result page
define('_FLD_ITEM_CODE', 'Itemcode');
define('_FLD_ITEM_NAME', 'Itemnaam');
define('_FLD_ITEM_QTY', 'Hoeveelheid');
define('_FLD_ITEM_PRICE', 'Prijs');
define('_FLD_ITEM_SUB_TOTAL', 'Subtotaal');
define('_FLD_ITEM_PAYMENT_METHOD', 'betalingsmethode');
define('_TERMS_N_CONDITIONS', 'Ik heb de <a style="color:#0092D2;text-decoration:underline;font-style:normal;" href="javascript:void(null);" id="showTerms">algemene voorwaarden</a> gelezen.');
define('_JS_MSG_MUST_ACCEPT', 'U moet instemmen met algemene voorwaarden<');
define('_JS_MSG_ADCONFIRMATION_MSG', 'Weet u het zeken?');

// thank you page
define('_THANK_YOU', 'Bedankt');
define('_PRINT_INVOICE', 'Factuur afdrukken');
define('_DEAR', 'Beste');
define('_THANK_YOU_MSG', "Bedankt voor uw aankoop. Uw bestelling is geplaatst.  Wij zullen uw bestelling binnenkort bij u bezorgen.");

// sorry page
define('_SORRY_PAYMENT_UNSUCCESSFUL', 'Betaling mislukt.');
define('_SORRY_MSG', 'Helaas uw bestelling is niet voltooid. Er zijn nog bestelde items in uw winkelwagen. U kunt uw bestelling op een latere tijdstip voltooien.  Onze excuses voor het ongemak.');

// wishlist
define('_MY_WISHLIST', 'Mijn Wishlist');
define('_JS_MSG_REMOVE_ITEM', 'Weet u het zeker?');
define('_VLD_JS_MSG_EMAIL_REQUIRED', 'Emailadres is verplicht.');
define('_COL_SIZE', 'Maat');
define('_COL_PRODUCT', 'Product');
define('_COL_DATE_ADDED', 'Datum toegevoegd');
define('_MSG_RECOMMENDED_SUCCESSFULLY', 'Wishlist verstuurd.');
define('_BTN_SEND', 'Send');
define('_RECOMMEND_FRIEND_LINK', 'Wishlist versturen');
define('_MSG_MAIL_SEND', 'Mail recommeded successfully.');

// my orders
define('_MY_ORDERS', 'Mijn bestellingen');
define('_FLD_ORDER_CODE', 'bestelnr.');
define('_FLD_DATE', 'Datum');
define('_FLD_GRAND_TOTAL', 'Bedrag');
define('_FLD_STATUS', 'Status');
define('_FLD_ORDER_DETAILS', 'Details');
define('_MSG_NO_ORDERS', 'U heeft geen bestellingen geplaatst.');

// change password
define('_CHANGE_PASSWORD', 'Wachtwoord wijzigen');
define('_CP_EMAIL', 'Emailadres');
define('_CP_CURRENT_PWD', 'Huidig wachtwoord');
define('_CP_NEW_PWD', 'Nieuwe wachtwoord');
define('_CP_CNEW_PWD', 'Bevestig wachtwoord');
define('_CP_MSG_SUCCEESS', 'wachtwoord gewijzigd.');
define('_BTN_CHANGE', 'Wijzigen');
define('_VLD_INVALID_CURRENT_PASSWORD', 'Huidig wachtwoord onjuist');
define('_VLD_INVALID_CONFIRM_PASSWORD_NOT_MATCHED', 'wachtwoord komt niet overeen');
define('_VLD_PROBLEM_CHANGE', 'wachtwoord wijziging mislukt!');

// my account
define('_BTN_SAVE_CHANGES', 'Opslaan');
define('_MY_ACCOUNT_MSG_SUCCESS', 'Opgeslagen');

// added later
define('_AVAOLABLE_COLORS', 'Beschikbaar kleuren');
define('_MSG_COLOR_ALERT', 'Selecteer kleur');
define('_PRODUCT_INFO', 'Product info');
define('_VIDEOS', 'Videos');
define('_BANK_DETAILS', 'BankNaam<br />Rek.nr: 123456789<br />t.n.v. agro trading te Barendrecht');
define('_INVOICE_NOTE', 'Uw bestelling wordt verwerkt zodra uw betaling is binnengekomen');

define('_INVOICE_FOOTER_NOTE', "Dank voor je bestelling bij agro trading. Op al onze leveringen zijn onze algemene voorwaarden van toepassing---");
define('_INVOICE_FOOTER_ADDRESS', "agrotrading<br />info@agrotrading.eu");

define('_BTN_BACK', 'Terug');

// filteration
define('_FILTER_BRAND', 'Filter op merk');
define('_FILTER_SERIES', 'Filter op serie');
define('_FILTER_DELIVERY', 'Filter op levertijd');
define('_FILTER_PRICE', 'Filteren op ');
define('_ALL_BRANDS', 'Alle merken');
define('_ALL_SERIES', 'Alle Serie');
define('_ALL_DELIVERIES', 'Levertijd');
define('_WITHOUT_SORTING', 'Geen sortering');
define('_SORT_ASCE', 'Oplopend');
define('_SORT_DESC', 'Aflopend');

// paginations
define('_PAGING_PAGES', 'Paginas');
define('_PAGING_DISPLAYING', '&nbsp;');
define('_PAGING_OF', 'van');

// offerte
define('_OFFERTE_DETAILS', 'Vul de offertelijst zo volledig mogelijk in. Om snel op uw aanvraag te kunnen reageren willen wij met name graag het merk en artikelnummer weten.');
define('_OFFERTE_SUCCESS', 'Offerte verstuurd');
define('_OFFERTE_PERSONAL', 'Persoonsgegevens');
define('_OFFERTE_NAAM', 'Voorvoegsel en achternaam');
define('_OFFERTE_Name', 'Naam');

define('_OFFERTE_GENDER_M', 'Dhr.');
define('_OFFERTE_GENDER_F', 'Mevr.');

define('_OFFERTE_EMAIL', 'Emailadres');
define('_OFFERTE_TELE', 'Telefoonnummer');
define('_OFFERTE_MOBILE', 'Mobiel');

define('_OFFERTE_PRODUCT_INFO', 'Producten informatie');

define('_OFFERTE_QTY', 'Aantal');
define('_OFFERTE_BRAND', 'Merk');
define('_OFFERTE_INFO', 'Productomschrijving');
define('_OFFERTE_SIZE', 'Afmeting');
define('_OFFERTE_COLOR', 'Kleur');
define('_OFFERTE_PRD_NO', 'Artikelnr.');

define('_OFFERTE_DESC', 'Opmerkingen');
define('_OFFERTE_DESC_VRAGEN', 'Opmerkingen / Vragen');

define('_OFFERTE_BTN_ADDNEW', 'Toevoegen');
define('_OFFERTE_BTN_QUOTE', 'Offerte aanvraag verzenden');

define('_OFFERTE_SUBJECT', 'Offerte aanvraag');
define('_OFFERTE_VLD_PRODUCTS', 'Invullen ' . _OFFERTE_QTY . ', ' . _OFFERTE_BRAND . ', ' . _OFFERTE_INFO . ' en ' . _OFFERTE_PRD_NO . ' tenminste voor een product.');

define('_NO_PRODUCTS_FOUND', 'Geen prduckten gevonden.');

define('_LEFT_AANBIEDINGEN', 'AANBIEDINGEN');

define('_PRD_ADVIESPRIJS', 'Adviesprijs');
define('_PRD_ACTIEPRIJS', ' Actieprijs');
define('_PRD_ONZE_PRIJS', ' Prijs');
define('_PAYMENT_INVOICE_TEXT_MESSAGE', 'Please print your invoice. After you make the payment into our account: <br />Rabobank<br />A/C number: 123456789<br />account name: agro trading of Barendrecht<br /> then your order will be dispatched.');
define('_PAYMENT_FACTUUR', ' Factuur');

// Product Detail
define('_Zoom', 'Groot');


//Sitemape
define('_SITEMAP_', 'Sitemap');


//Disclaimer
define('_DISCLAIMER', 'Disclaimer');

// Other
define('_ORDER_NOW_THE', 'Bestel nu de ');
define('_PRINT_PAGE', 'Print deze pagina');
define('_MEERINFO', 'MEER INFO');
