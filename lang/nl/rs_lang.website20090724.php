<?php
// title
define('_META_TITLE', "|| Sanihuis Sanitair ||");

// header
define('_CHOOSE_LANGUAGE', 'Kies een taal:');
define('_REGISTER_NOW', 'Aanmelden!!');
define('_LOGIN', 'Inloggen');
define('_HOME', 'Home');
define('_WELCOME', 'Welkom');
define('_LOGOFF', 'Uitloggen');
define('_MY_ACCOUNT', 'Mijn Account');
define('_BTN_ZOEKEN', 'Zoeken');
define('_CHANGE_PASSWORD', 'Watchwoord Wijzigen');

// menu
define('_ALL_PRODUCTS', 'All Products');
define('_OVER_ONS', 'Over ons');
define('_WEBSHOP', 'Webshop');
define('_SHOWROOM', 'Showroom');
define('_TARIEVEN', 'Tarieven');

define('_INSTALLATIE', 'Installatie');
define('_SERVICE', 'Service');
define('_ZOEKEN', 'Zoeken');
define('_CONTACT', 'Contact');
define('_OFFERTE', 'Offerte');

define('_VIEW_SHOPPING_BAG', 'Winkelmandje');
define('_NUM_OF_ITEMS', 'items');
define('_MSG_WISHLIST_EMPTY', 'Uw wishlist is leeg.');
// left
define('_PRODUCTS', 'Producten');

// footer
define('_COPYRIGHTS', "&copy; 2009 Sanihuis Sanitair. All right reserved.");
define('_TERMS', 'Algemene voorwaarden');
define('_PRIVACY', 'Privacy Policy');
define('_POWERED_BY', 'Powered by: <a target="_blank" href="http://www.webciters.com/">WEBCITERS</a>');

// home page
define('_HOME_PRODUCT', 'Home Products');
define('_VIEW_DETAILS', 'Details');
define('_ADD_TO_CART', 'Naar de winkelmand');
define('_ADD_TO_WISHLIST', 'Naar de wishlist');
define('_PRIVACY', 'Privacy Policy');
define('_HOME_SLOGAN', "Sanihuis Sanitair");

// 404 page
define('_ACCESS_DENIED', 'Access Denied!');
define('_PAGENOT_FOUND_MSG', 'You have no access to use this page or page not found. Please contact administrator.');

// collection
define('_PRODUCTS', 'Producten');
define('_PRODUCT_LIST', 'Productlijst');

// product details page
define('_PRODUCT_DETAILS', 'Product details');
define('_SELECT_SIZE', 'Selecteren');
define('_MSG_SELECT_PRODUCT_SIZE', 'Selecteer een maat');
define('_MSG_PRODUCT_SOLD_OUT', 'Uitverkocht');
define('_SOLD_OUT', 'Uitverkocht');

// newsletter
define('_SEX', 'Geslacht');
define('_SEX_MALE', 'Man');
define('_SEX_FEMALE', 'Vrouw');
define('_INITIALS', 'Voorletters');
define('_NAME', 'Naam');
define('_EMAIL', 'E-mail');
define('_HOMETOWN', 'Plaats');
define('_SUBSCRIBE', 'Aanmelden');
define('_UNSUBSCRIBE', 'Afmelden');
define('_VLD_EMAIL', 'E-mail is verplicht veld.');
define('_VLD_NAME', 'Naam is verplicht veld.');
define('_VLD_INITIALS', 'Voorletters is verplicht veld.');
define('_VLD_HOMETOWN', 'Plaats is verplicht veld.');
define('_MSG_ALREADY_SUBSCRIBED', 'U bent al aangemeld!');
define('_MSG_SUCCESSFULLY_SUBSCRIBED', 'Your subscription information has been saved successfully. However, you need to confirm your subscription. Please check your email for further details.');
define('_MSG_UNSCRIBPTION_NEED_CONFIRM', 'You need confirm your unsubscription. Please check your email for further details.');
define('_EMAIL_NOT_FOUND', 'E-mail could not be found.');
define('_SUBSCRIBED_AND_ACTIVATED', 'You have been successfully subcribed and activated your account.');
define('_UNSUBSCRIBED_SUCCESSFUL', 'You have been successfully unsubcribed and your email address has been deleted from our database.');

// media
define('_READ_MORE', 'Lees meer');

// news details
define('_NEWS_DETAILS', 'Nieuws Details');

// contact
define('_YOUR_INFO', 'Your Information');
define('_E_MAIL', 'E-mail');
define('_FULLNAME', 'Naam');
define('_ADDRESS', 'Adres');
define('_COUNTRY', 'Land');
define('_SUBJECT', 'Subject');
define('_MESSAGE', 'Message');
define('_SECURITY_CODE', 'Security Code');
define('_BTN_SUBMIT', 'Submit');
define('_BTN_CANCEL', 'Cancel');
define('_SELECT_COUNTRY', 'Select Country');

define('_CONT_VLD_EMAIL', 'E-mail is invalid.');
define('_CONT_VLD_FULLNAME', 'Name is a required field.');
define('_CONT_VLD_COUNTRY', 'Country is a required field.');
define('_CONT_VLD_SUBJECT', 'Subject is a required field.');
define('_CONT_VLD_MESSAGE', 'Message is a required field.');
define('_CONT_VLD_SECURITY_CODE', 'Security Code is a required field.');
define('_CONT_VLD_INVALID_SECURITY_CODE', 'Invalid Security Code!');
define('_CONT_TOP_MESSAGE', "Following message has been posted from the contact page of Sanihuis.");
define('_CONT_SUBMITTED_SUCCESSFULLY', 'Thank you for contacting us. <br />Contact information has been submitted successfully. We will get back to you as soon as possible.');

// cart
define('_VIEW_CART', 'Shopping Bag');
define('_MSG_REMOVE_ITEM', 'Are you sure you want to remove the item from the cart?');
define('_MSG_QTY_POSITIVE', 'Quantity must be a positive value.');
define('_COL_DESCRIPTION', 'Description');
define('_COL_QTY', 'Quantity');
define('_COL_PRICE', 'Price');
define('_COL_TOTAL', 'Total');
define('_LNK_VIEW_DETAILS', 'View Details');
define('_LNK_REMOVE_ITEM', 'Remove');
define('_COL_ITEM_TOTAL', 'Item Total');
define('_COL_GRAND_TOTAL', 'Grand Total');
define('_EMPTY_CART', 'Your shopping bag is empty.');
define('_WE_ACCEPT', 'We accept');

define('_BTN_CONTINUE_SHOPPING', 'Continue Shopping');
define('_BTN_UPDATE_CART', 'Update Cart');
define('_BTN_CHECKOUT', 'Proceed to Checkout');

// login
define('_LOGIN_INFO', 'Login to your account');
define('_LOGIN_EMAIL', 'E-mail');
define('_LOGIN_PWD', 'Password');
define('_LOGIN_BTN', 'Login');
define('_LOGIN_FORGOT', 'Forgot your password?');
define('_CHANGE_PASSWORD', 'Change Password');

define('_LOGIN_NEW', "New to Sanihuis?");
define('_LOGIN_NEW_MSG', "Are you new to Sanihuis Sanitair? Don't worry. A few easy steps will complete your registration into Sanihuis. Just click the button below to proceed.");
define('_LOGIN_FORGOT', 'Forgot Password?');
define('_LOGIN_FORGOT_MSG', 'Forgot password? Don\'t worry you can retrieve your password within a few seconds.');
define('_LOGIN_LNK_REGISTER_NOW', 'Register Now');
define('_LOGIN_LNK_REQUEST_NOW', 'Request Now');

define('_VLD_INVALID_EMAIL', 'E-mail is a required field.');
define('_VLD_PASSWORD', 'Password is a required field.');
define('_LOGIN_INVALID_LOGIN', 'Invalid login information. Please try again.');
define('_CUST_ACCOUNT_SUSPENDED', 'Your account has been suspended. Contact administrator.');

// forgot
define('_FORGOT_PASWORD', 'Forgot Password');
define('_FORGOT_EMAIL_NOT_VALID', 'E-mail address does not exists.');
define('_FORGOT_CUSSESS', 'Your password has been reset and sent to your email address. Please check yor email. You can now <a href="' . SITE_URL . '?show=login">login</a> with new password.');

// register
define('_REGISTRATION', 'Registratie');
define('_REG_SUCCESS', 'Je bent geregistreerd. Je kan nu inloggen en doorgaan met winkelen.');
define('_REG_LOGIN_INFO', 'Login informatie');
define('_REG_EMAIL', 'Email adres');
define('_REG_PASSWD', 'Wachtwoord');
define('_REG_CONFIRM_PASSWD', 'Bevestig wachtwoord');
define('_REG_PERSONAL_INFO', 'Persoonlijke gegevens');
define('_REG_TITLE', 'Aanhef dhr/ mevr.');
define('_REG_FIRST_NAME', 'Voornaam');
define('_REG_LAST_NAME', 'Achternaam');
define('_REG_BILLING_INFO', 'Factuur gegevens');
define('_REG_ADD_1', 'Straatnaam en huisnummer');
define('_REG_ADD_2', 'Address 2');
define('_REG_CITY', 'Plaats');
define('_REG_COUNTY', 'Provincie');
define('_REG_ZIP', 'Postal/Zip');
define('_REG_COUNTRY', 'Land');
define('_REG_DAY_PHONE', 'Telefoon overdag');
define('_REG_MOBILE', 'Mobiel');
define('_REG_SAME_BILLING', 'Mijn factuur adres is anders dan het bezorg adres');
define('_REG_SHIPPING_INFO', 'Bezorg informatie');
define('_REG_SECURITY', 'Beveiliging');
define('_REG_SECURITY_CODE', 'Beveiligingscode');
define('_REG_REGISTER', 'Register');
define('_REG_INVALID_SECURITY_CD', 'Invalid Security Code!');
define('_REG_CONFIRM_PWD_NOT_MATCHED', 'Confirmed password does not match.');
define('_REG_EMAIL_ALREADY_EXISTS', 'E-mail adres al in gebruik.');

define('_IS_REQUIRED_FLD', ' is a required field.');

// checkout
define('_CHECKOUT', 'Checkout');
define('_FREE', 'Free');
define('_CHECKOUT_CONTACT', 'Contact');
define('_BILLING_ADDRESS', 'Factuur adres');
define('_ORDER_DATE', 'Order Date');
define('_SHIPPING_ADDRESS', 'Bezorg adres');
define('_CHECKOUT_CHANGE', 'Change');
define('_JS_MSG_SELECT_SHIPPING_TYPE', 'Please select shipping type.');
define('_JS_MSG_SELECT_PAYMENT_METHOD', 'Please select payment method: \n- iDEAL \n- PayPal \n- Advance Payment \n- Cash on Delivery');
define('_SELECT_SHIPPING_TYPE', 'Select Shipping Type');
define('_PAYMENT_IDEAL', 'iDEAL');
define('_PAYMENT_PAYPAL', 'Paypal');
define('_PAYMENT_ADV', 'Vooruit betalen');
define('_PAYMENT_CASH_ON_DELIVERY', 'Cash on Delivery');

// ideal payment page
define('_IDEAL_CHECKOUT', 'iDEAL Checkout');
define('_IDEAL_SHIPPING', 'Shipping');
define('_IDEAL_DELIVERY_TIME', 'Delivery Time');
define('_IDEAL_SHIPPING_CHARGE', 'Shipping Charge');
define('_FREE_BEFORE', 'Free for greater than');
define('_FREE_AFTER', 'purchase');

define('_TOTAL_PURCHASE_ITEMS', 'Total Purchased Items');

// paypal checkout page
define('_PAYPAL_CHECKOUT', 'Paypal Checkout');

define("_JS_FORM_ERROR", "Please do the following:");

// advanced payment page
define('_ADV_CHECKOUT', 'Vooruit betalen - Checkout');
define('_ADV_BANK_DETAILS', 'Bank Details');
define('_ADV_BANK_NAME', 'Naam bank');
define('_ADV_ACC_NAME', 'Naam begunstigde');
define('_ADV_ACC_NUM', 'Rekeningnummer');
define('_ADV_AGREEMENT_TEXT', "Ik ga akkoord met vooruitbetaling aan Sanihuis van mijn rekening.");

define('_JS_MSG_ADV_BANK_NAME', 'Bank Name is a required field.');
define('_JS_MSG_ADV_ACC_NAME', 'Account Name is a required field.');
define('_JS_MSG_ADV_ACC_NUM', 'Account Number is a required field.');
define('_JS_MSG_ADV_AGREEMENT', "You must agree on that Sanihuis can take advance from your account.");

// cash on delivery page
define('_CASH_ON_DELIVERY', 'Rembours - Checkout');

// payment response/result page
define('_FLD_ITEM_CODE', 'Item Code');
define('_FLD_ITEM_NAME', 'Item Name');
define('_FLD_ITEM_QTY', 'Hoeveelheid');
define('_FLD_ITEM_PRICE', 'Prijs');
define('_FLD_ITEM_SUB_TOTAL', 'Sub Totaal');
define('_FLD_ITEM_PAYMENT_METHOD', 'Payment Method');
define('_TERMS_N_CONDITIONS', 'Ik heb de <a style="color:yellow;text-decoration:underline;font-style:normal;" href="javascript:void(null);" id="showTerms">algemene voorwaarden</a> gelezen.');
define('_JS_MSG_MUST_ACCEPT', 'You must accept terms and conditions.');
define('_JS_MSG_ADCONFIRMATION_MSG', 'Are you sure about the bank details and want to order with Advanced Payment ?');

// thank you page
define('_THANK_YOU', 'Thank you');
define('_PRINT_INVOICE', 'Print Invoice');
define('_DEAR', 'Dear');
define('_THANK_YOU_MSG', "Thank you very much for purchasing items from Sanihuis. Your order has been placed successfully. As per your request, the items that you have purchased will be delivered in time.");

// sorry page
define('_SORRY_PAYMENT_UNSUCCESSFUL', 'Payment Unsuccessful');
define('_SORRY_MSG', 'Sorry your order could not be completed successfully. Your basket still has the purchased items. You can complete your purchase at a later time. Sorry for the incovenience.');

// wishlist
define('_MY_WISHLIST', 'My Wishlist');
define('_JS_MSG_REMOVE_ITEM', 'Are you sure to remove the item from wishlist');
define('_VLD_JS_MSG_EMAIL_REQUIRED', 'E-mail Address is a required field.');
define('_COL_SIZE', 'Size');
define('_COL_PRODUCT', 'Product');
define('_COL_DATE_ADDED', 'Date Added');
define('_MSG_RECOMMENDED_SUCCESSFULLY', 'Wishlist recommeded successfully.');
define('_BTN_SEND', 'Send');
define('_RECOMMEND_FRIEND_LINK', 'Send Wishlist to Friend');

// my orders
define('_MY_ORDERS', 'My Orders');
define('_FLD_ORDER_CODE', 'Order Code');
define('_FLD_DATE', 'Date');
define('_FLD_GRAND_TOTAL', 'Grand Total');
define('_FLD_STATUS', 'Status');
define('_FLD_ORDER_DETAILS', 'Order Details');
define('_MSG_NO_ORDERS', 'There are no orders made by you so far.');

// change password
define('_CHANGE_PASSWORD', 'Change Password');
define('_CP_EMAIL', 'E-mail');
define('_CP_CURRENT_PWD', 'Current Password');
define('_CP_NEW_PWD', 'New Password');
define('_CP_CNEW_PWD', 'Bevestig wachtwoord');
define('_CP_MSG_SUCCEESS', 'Password changed successfully.');
define('_BTN_CHANGE', 'Change');
define('_VLD_INVALID_CURRENT_PASSWORD', 'Invalid current password.');
define('_VLD_INVALID_CONFIRM_PASSWORD_NOT_MATCHED', 'Confirmed password does not match.');
define('_VLD_PROBLEM_CHANGE', 'Problem while changing the password.');

// my account
define('_BTN_SAVE_CHANGES', 'Save Changes');
define('_MY_ACCOUNT_MSG_SUCCESS', 'Your account information is saved.');

// added later
define('_AVAOLABLE_COLORS', 'Available Colors');
define('_MSG_COLOR_ALERT', 'Please select a color.');
define('_PRODUCT_INFO', 'Product Info');
define('_VIDEOS', 'Videos');
define('_BANK_DETAILS', 'Rabobank<br />Rek.nr: 148477429<br />t.n.v. Ichtiram bv te Abcoude');
define('_INVOICE_NOTE', 'Uw order wordt verwerkt zodra uw betaling is binnengekomen');

define('_INVOICE_FOOTER_NOTE', "Dank voor je bestelling bij Sanihuis. Op al onze leveringen zijn onze algemene voorwaarden van toepassing (deze staan op www.ichtiram.com). Vul bij ruilen het Retourformulier in.---");
define('_INVOICE_FOOTER_ADDRESS', "Sanihuis, Postbus 45, 5600AA, Eindhoven, Nederland/ the Netherlands/ Pays-Bas<br />KvKno.: 30259986, Rabobank 148477429<br />info@sanihuis.nl");

define('_BTN_BACK', 'Back');

// filteration
define('_FILTER_BRAND', 'Filter op merk');
define('_FILTER_SERIES', 'Filter op serie');
define('_FILTER_DELIVERY', 'Filter op levertijd');
define('_FILTER_PRICE', 'Filter op prijs');
define('_ALL_BRANDS', 'All Brands');
define('_ALL_SERIES', 'All Series');
define('_ALL_DELIVERIES', 'All Deliveries');
define('_WITHOUT_SORTING', 'Without Sorting');
define('_SORT_ASCE', 'Ascending');
define('_SORT_DESC', 'Descending');

// paginations
define('_PAGING_PAGES', 'Pages');
define('_PAGING_DISPLAYING', 'Displaying');
define('_PAGING_OF', 'of');

// offerte
define('_OFFERTE_DETAILS', 'Vul de offertelijst zo volledig mogelijk in. Om snel op uw aanvraag te kunnen reageren willen wij met name graag het merk, artikelnummer en kleur weten.');
define('_OFFERTE_SUCCESS', 'Quatation has been sent succcessfully.');
define('_OFFERTE_PERSONAL', 'Persoonsgegevens');
define('_OFFERTE_NAAM', 'Voorvoegsel en achternaam');

define('_OFFERTE_GENDER_M', 'Dhr.');
define('_OFFERTE_GENDER_F', 'Mevr.');

define('_OFFERTE_EMAIL', 'E-Mailadres');
define('_OFFERTE_TELE', 'Telefoonnummer');
define('_OFFERTE_MOBILE', 'Mobiel');

define('_OFFERTE_PRODUCT_INFO', 'Producten Information');

define('_OFFERTE_QTY', 'Aantal');
define('_OFFERTE_BRAND', 'Merk');
define('_OFFERTE_INFO', 'Productomschrijving');
define('_OFFERTE_SIZE', 'Afmeting');
define('_OFFERTE_COLOR', 'Kleur');
define('_OFFERTE_PRD_NO', 'Artikelnr.');

define('_OFFERTE_DESC', 'Opmerkingen');
define('_OFFERTE_DESC_VRAGEN', 'Opmerkingen / Vragen');

define('_OFFERTE_BTN_ADDNEW', 'Add New');
define('_OFFERTE_BTN_QUOTE', 'Offerte aanvraag verzenden');

define('_OFFERTE_SUBJECT', 'Request for Quatation');
define('_OFFERTE_VLD_PRODUCTS', 'Please fill ' . _OFFERTE_QTY . ', ' . _OFFERTE_BRAND . ', ' . _OFFERTE_INFO . ' and ' . _OFFERTE_PRD_NO . ' at least for one product.');

?>