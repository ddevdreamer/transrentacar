<?php
$objGetCurrentCat = new Product;
$objGetCurrentCat->setProperty("url_key", $_GET["category_id"]);
$objGetCurrentCat->lstCategory();
$GetCurrentCat = $objGetCurrentCat->dbFetchArray(1);
?>  
<main>

<div class="container">
    <div class="ct-breadcrumbs-container">
        <div class="ct-breadcrumbs ct-js-breadcrumbs" data-bg-image="<?php echo SITE_URL;?>assets/images/demo-content/breadcrumbs3.png" data-bg-scratchImage="<?php echo SITE_URL;?>assets/images/scratch.png" data-height="260">
            <div class="ct-breadcrumbs-title">
                <?php echo $GetCurrentCat["category_name"];?>
            </div>
        </div>

        <ol class="breadcrumb  ">
            <li><a href="<?php echo SITE_URL;?>">Home</a></li>
            <li class="active"><?php echo $GetCurrentCat["category_name"];?></li>
        </ol>
    </div>
</div>



<section>
<div class="container">
<div class="ct-gallery ct-portfolio-container ct-portfolio-masonry ct-portfolio-masonry--col3">


 <?php
            $objProductList = new Product;
			$objProductImg = new Product;
			$objCatDetail = new Product;
			$objProductList->setProperty("category_id", $GetCurrentCat["category_id"]);
            $objProductList->lstProducts();
            while($ProList = $objProductList->dbFetchArray(1)){
				
				$objCatDetail->setProperty("category_id", $ProList["category_id"]);
				$objCatDetail->lstCategory();
				$CatDetail = $objCatDetail->dbFetchArray(1);
				
				$objProductImg->setProperty("product_id", $ProList["product_id"]);
				$objProductImg->lstProductGallery();
				$ProductImg = $objProductImg->dbFetchArray(1);
				if($ProductImg['file_name']!=''){
            ?>

    <article class="ct-gallery-item ct-portfolio-item">

        <div class="ct-portfolio-item-outer">
            <div class="ct-portfolio-item-media">
                <figure>
                    <a href="#"><img src="<?php echo SITE_URL.'product_image/orig/'.$ProductImg['file_name'];?>" alt=""/></a>
                </figure>
            </div>

            <div class="ct-portfolio-item-wrapper">
                <div class="ct-portfolio-item-inner">
                    <section>
                        <div class="ct-portfolio-item-title">
                            <a href="#">
                                <h6><span><?php echo $ProList['product_name'];?></span></h6>
                            </a>
                        </div>

                        <div class="text-center">
                            <a href="#" class="btn ct-btn-image btn-warning"><span><i class="fa fa-book"></i> Read More</span></a>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </article>

<?php } } ?>


</div>
</div>
</section>

<div class="container ct-u-marginTop10 ct-u-marginBottom10">
    <div class="ct-divider ct-divider--big"></div>
</div>






</main>