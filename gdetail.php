<?php
require_once("config/config.php");
$objCommon 		= new Common;
$objMail 		= new Mail;
$objContent 	= new Content;
$objValidate 	= new Validate;
$objTemplate 	= new Template;
$objAdminUser 	= new AdminUser;
$GetProjectid = $_GET["pi"];

$objProject = new Content;
$objProject->setProperty("portfolio_id", $GetProjectid);
$objProject->lstProject();
$ProjectList = $objProject->dbFetchArray(1);
?>
<div id="single-portfolio">
	<div id="portfolio-details" class="container">
		<a class="close-folio-item" href="#"><i class="fa fa-times"></i></a>
		<img class="img-responsive" src="<?php echo SITE_URL.'project_img/'.$ProjectList["project_file_2"];?>" alt="">
		<div class="row">
			<div class="col-sm-9">
				<div class="project-info">
					<h3><?php echo $ProjectList["project_name"];?></h3>
					<?php echo $ProjectList["project_description"];?>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="project-details">
					<h3>Project Details</h3>
                    <?php if($ProjectList["client_name"]!=''){?>
					<p><span>Client: </span><?php echo $ProjectList["client_name"];?></p>
                    <?php } ?>
					<p><span>Tag:</span> <?php echo $ProjectList["project_tags"];?></p>
				</div>  
			</div>
		</div>
	</div>
</div>